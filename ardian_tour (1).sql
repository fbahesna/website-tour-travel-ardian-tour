-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2019 at 04:56 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ardian_tour`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `about` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `judul`, `about`) VALUES
(0, 'About', 'Ardian Tour merupakan perusahaan yang bergerak di Bidang Jasa Biro Perjalan Tour yang berpusat di Purworejo sejak awal tahun 2014.');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `tujuan_wisata` varchar(120) NOT NULL,
  `date_created` varchar(30) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `durasi` int(11) NOT NULL,
  `voucher` varchar(225) DEFAULT NULL,
  `date` varchar(20) NOT NULL,
  `notelepon` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `namalengkap` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkout_promo`
--

CREATE TABLE `checkout_promo` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `notelepon` int(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `potongan_promo` int(11) NOT NULL,
  `tujuan_promo` varchar(100) NOT NULL,
  `durasi_promo` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_bayar` int(15) NOT NULL,
  `harga_normal` int(11) NOT NULL,
  `date_created` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id` int(11) NOT NULL,
  `nama` varchar(122) NOT NULL,
  `email` varchar(122) NOT NULL,
  `notelp` varchar(122) NOT NULL,
  `alamat` varchar(122) NOT NULL,
  `tujuan` varchar(122) NOT NULL,
  `durasi` int(122) NOT NULL,
  `keberangkatan` varchar(122) NOT NULL,
  `date_created` varchar(122) NOT NULL,
  `peserta` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `voucher` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_user`
--

CREATE TABLE `menu_user` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_user`
--

INSERT INTO `menu_user` (`id`, `menu`) VALUES
(1, 'Administrator'),
(2, 'User'),
(3, 'Menu');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` int(11) NOT NULL,
  `email` varchar(122) NOT NULL,
  `subject` varchar(122) NOT NULL,
  `pesan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `email`, `subject`, `pesan`) VALUES
(3, 'admin@admin.com', 'new member', 'fdsaewf'),
(4, 'fdfdsf@fdfefef.com', 'fdraevr', 'frwvarwf'),
(5, 'fbahezna@gmail.com', 'wew', 'Hallo ardian');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `nama_promo` varchar(125) NOT NULL,
  `bulan_promo` varchar(125) NOT NULL,
  `harga_promo` char(14) NOT NULL,
  `image` varchar(255) NOT NULL,
  `pesan_promo` varchar(100) NOT NULL,
  `potongan_promo` int(20) NOT NULL,
  `durasi_promo` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id`, `nama_promo`, `bulan_promo`, `harga_promo`, `image`, `pesan_promo`, `potongan_promo`, `durasi_promo`) VALUES
(33, 'Prau', '24-27 Juli 2019', '2500000', 'Prau1.jpg', 'Prau', 500000, '4D3N'),
(36, 'Magelang', 'Agustus 1-20 2019', '1000000', 'promoMagelang.jpg', 'Magelang Exotic', 260000, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket`
--

CREATE TABLE `tbl_paket` (
  `id` int(11) NOT NULL,
  `paket_tour` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `image` varchar(255) NOT NULL,
  `one_day` varchar(100) DEFAULT NULL,
  `n2D1N` varchar(100) DEFAULT NULL,
  `n3D2N` varchar(100) DEFAULT NULL,
  `n4D3N` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_paket`
--

INSERT INTO `tbl_paket` (`id`, `paket_tour`, `tujuan`, `keterangan`, `image`, `one_day`, `n2D1N`, `n3D2N`, `n4D3N`) VALUES
(7, 'Dieng & Sikunir', '-Bukit sikunir -Kawah Sikidang -Telaga Warna -Batu ratapan angin -Candi arjuna -Dieng Theater -Telaga cebengan', 'Bukit Sikunir Dieng, atau lebih tepatnya Puncak Sikunir merupakan salah satu destinasi favorit yang ada di Dataran Tinggi Dieng. Terletak di Wonosobo, Jawa Tengah bukit ini berada di desa tertinggi di Pulau Jawa, yaitu Desa Sembungan yang mempunyai ketinggian 2.306 MDPL. Keindahan golden sunrise-nya selalu dinanti wisatawan lokal maupun mancanegara setiap harinya.', 'Dieng.jpg', '1500000', '2500000', '3000000', '4000000'),
(8, 'Gunung prau & Dieng', '-Telaga warna -Batu ratapan angin -Candi Arjuna -Gunung prau -Puncak Prau', 'Gunung prau dieng, baru-baru ini menjadi primadona baru bagi para pendaki gunung. Padahal jika di bandingkan dengan pegunungan lain dipulau jawa, gunung prau ini memiliki ketinggian yang sedang dengan jalur yang tidak begitu sulit.', 'Prau.jpg', '1100000', '2100000', '3000000', '4100000'),
(9, 'Bromo ', '-Gunung bromo -Pananjakan sunrise -bukit teletabies| savana -pasir berbisik -Kawah bromo -JEEP -Pura luhur poten -Widodaren -Gunung Batok -Kampung warna -Bukit Cinta -Coban Pelangi -Candi jago -Bukit kingkong', 'Gunung Bromo (dari bahasa Sanskerta: Brahma, salah seorang Dewa Utama dalam agama Hindu) atau dalam bahasa Tengger dieja \"Brama\", adalah sebuah gunung berapi aktif di Jawa Timur, Indonesia. Gunung ini memiliki ketinggian 2.329 meter di atas permukaan laut dan berada dalam empat wilayah kabupaten, yakni Kabupaten Probolinggo, Kabupaten Pasuruan, Kabupaten Lumajang, dan Kabupaten Malang. Gunung Bromo terkenal sebagai objek wisata utama di Jawa Timur. Sebagai sebuah objek wisata, Bromo menjadi menarik karena statusnya sebagai gunung berapi yang masih aktif. Gunung Bromo termasuk dalam kawasan Taman Nasional Bromo Tengger Semeru.', 'Bromo.jpg', '1200000', '2200000', '3000000', '4200000'),
(10, 'Nusa Penida', '-Broken beach -Angels Billabong -Keliling Cliff -Pohon cinta -Christal Bay -Atuh beach -Pantai Pasir UUG', 'Nusa Penida adalah sebuah pulau (=nusa) bagian dari negara republik indonesia yang terletak di sebelah tenggara Bali yang dipisahkan oleh Selat Badung. Di dekat pulau ini terdapat juga pulau-pulau kecil lainnya yaitu Nusa Ceningan dan Nusa Lembongan. Perairan pulau Nusa Penida terkenal dengan kawasan selamnya di antaranya terdapat di Penida Bay, Manta Point, Batu Meling, Batu Lumbung, Batu Abah, Toyapakeh dan Malibu Point.', 'nusa.jpg', '1300000', '2300000', '3300000', '4300000'),
(24, 'Ubud & Rice terrace', '-Kantomlampo waterfall -Goa rang reng waterfall -Tegalalang Rice Terrace -Ubud Market', 'Menyambangi Tegallalang Rice Terrace, Bali – Dalam mengisi waktu liburan singkat di Bali dan sesaat setelah menghabiskan waktu sekitar 30 menit bermain bersama monyet-monyet lucu di Sacred Monkey Forest Sanctuary Ubud, kami pun melanjutkan perjalanan menuju destinasi kedua kami yakni Tegallalang Rice Terrace yang juga masih berada di Ubud.', 'ubud.jpg', '4320000', '4420000', '4520000', '4620000'),
(25, 'Bali', '-Pantai Pandawa -Kuta -Jimbaran -Tanjung Benoa -Pura Tirta Empul -Pura Ulun Danu -Pura tanah lot -Air terjun Tegenungan -Ubud -Bedugul -Uluwatu -GWK', 'Bali adalah sebuah provinsi di Indonesia. Ibu kota provinsi ini adalah Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.', 'bali.jpg', '2000000', '3000000', '4000000', '5000000'),
(26, 'Lembang', '-The lodge maribaya -Dusun bambu - Farm house + floating mask - Bukit bintang pinus', 'Lembang adalah sebuah kecamatan di Kabupaten Bandung Barat, Jawa Barat, Indonesia. Kecamatan ini berjarak sekitar 22 Kilometer dari ibu kota kabupaten Bandung Barat ke arah timur laut melalui Cisarua. Pusat pemerintahannya berada di Desa Lembang. Kecamatan Lembang merupakan kecamatan paling timur dan terkenal sebagai tujuan wisata di Jawa Barat.', 'lembnag.jpg', '2000000', '3000000', '4000000', '5000000'),
(27, 'Banyuwangi & P.Menjangan', '-Red island -menjangan Island -kawah ijen -djawatan -teluk hijau -pantai rajeg wesi -osing deles -baluran  -tabuhan', 'Jika pantai-pantai di Banyuwangi selatan, seperti Pulau Merah, Plengkung dan Grajagan populer dengan olahraga surfing, maka di pantai di Banyuwangi utara seperti Bangsring dan Pulau Tabuhan yang kondisi lautnya lebih tenang, menjadi surganya bagi penggemar olahraga bawah air seperti menyelam dan snorkeling. Masih bisa ditambah satu lagi surga bawah laut lainnya, yaitu Pulau Menjangan.', 'banyuwangi.jpg', '2000000', '3000000', '4000000', '5000000'),
(28, 'Lombok', '-Pantai Kuta -Tanjungan/p.tanjung Aan -bukit marese Hill -gili kapal -gili nanggu -gili kedis -gili sudak -air terjun benang kelambu -air terjun benang skotel -taman armada -desa sade -desa sukarara -Gili Trawang -Monkey forest -Bukit malaka/malimbu -desa', 'Pulau Lombok (jumlah penduduk pada tahun 2001: 2.722.123 jiwa)[1] adalah sebuah pulau di kepulauan Sunda Kecil atau Nusa Tenggara yang terpisahkan oleh Selat Lombok dari Bali di sebelat barat dan Selat Alas di sebelah timur dari Sumbawa. Pulau ini kurang lebih berbentuk bulat dengan semacam \"ekor\" di sisi barat daya yang panjangnya kurang lebih 70 km. Luas pulau ini mencapai 5.435 km² menempatkannya pada peringkat 108 dari daftar pulau berdasarkan luasnya di dunia. Kota utama di pulau ini adalah Kota Mataram.', 'lombok.jpg', '2000000', '3000000', '4000000', '5000000');

-- --------------------------------------------------------

--
-- Table structure for table `uploadresi`
--

CREATE TABLE `uploadresi` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `gambar` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'numknight', 'admin@admin.com', 'default.jpg', '$2y$10$ma3uD22RSZiuw0ptUcrYVORfNzIecdQaJs1nJq/RecCUB1J/MZjW.', 1, 1, 1552388125),
(19, 'Lianum Frada Bahesna', 'fbahezna@gmail.com', 'default.jpg', '$2y$10$cCjv8bVQdmVrP8uAEz0foe6HiIVHcrM9IQKnTDfi3EIf0M18AfidS', 2, 1, 1566966954);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Tabel', 'Admin', 'fas fa-fw fa-table', 1),
(2, 1, 'Tambah data', 'Admin/inputdata', 'fas fa-fw fa-database', 1),
(4, 3, 'Menu Management', 'Menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(9, 1, 'Role', 'admin/role', 'fas fa-fw fa-user-slash', 1),
(14, 1, 'Daftar Pemesan', 'Pesan/tampilCheckout', ' fas fa-fw fa-shopping-cart  ', 1),
(15, 2, 'Beranda', 'user', 'fas fa-fw fa-user-slash', 1),
(17, 1, 'Settings Promo', 'promo/settingsPromo', 'fas fa-fw fa-edit', 1),
(18, 2, 'Promo', 'promo', 'fas fa-fw fa-tag', 1),
(19, 2, 'Ubah Password', 'user/ubahPassword', 'fas fa-fw fa-key', 1),
(20, 1, 'Daftar Pemesan Promo', 'admin/daftarPemesananPromo', 'fas fa-fw fa-shopping-cart', 1),
(21, 1, 'VOUCHER ', 'voucher', 'fas fa-fw fa-tag', 1),
(22, 2, 'Voucher', 'voucher/ambilVoucher', 'fas fa-fw fa-tag', 1),
(23, 1, 'Laporan Pesanan', 'pesan/laporanPesanan', 'fas fa-fw fa-book', 1),
(24, 1, 'Cek Email', 'about/cekEmail', 'far fa-fw fa-envelope', 1);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` varchar(15) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diskon` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `tanggal`, `diskon`) VALUES
('1507190001', '2019-07-15 09:08:50', 155000),
('1507190004', '2019-07-15 10:18:52', 75000),
('2607190001', '2019-07-26 12:10:38', 150000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout_promo`
--
ALTER TABLE `checkout_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_user`
--
ALTER TABLE `menu_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploadresi`
--
ALTER TABLE `uploadresi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `checkout_promo`
--
ALTER TABLE `checkout_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_user`
--
ALTER TABLE `menu_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `uploadresi`
--
ALTER TABLE `uploadresi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_user`
--
ALTER TABLE `menu_user`
  ADD CONSTRAINT `menu_user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_access_menu` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_access_menu` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
