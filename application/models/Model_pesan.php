<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pesan extends CI_Model

{
    public function validasiVoucher($diskon)
    {
        $totalpembayaran = (int) $this->input->post('durasi') * (int) $this->input->post('qty') - $diskon;
        $data = [
            "tujuan_wisata" => $this->input->post('tujuan_wisata', true),
            "namalengkap" => $this->input->post('namalengkap', true),
            "email" => $this->input->post('email', true),
            "notelepon" => $this->input->post('notelepon', true),
            "alamat" => $this->input->post('alamat', true),
            "date_created" => date("d-m-Y"),
            "durasi" => $this->input->post('durasi', true),
            "qty" => $this->input->post('qty', true),
            "date" => $this->input->post('start', true),
            "voucher" => $this->input->post('voucher', true),
            "total_bayar" =>  $totalpembayaran
        ];
        $this->db->insert('checkout', $data);
    }
    public function cekDataLaporan()
    {
        return $this->db->get_where('laporan', ['email' => $this->session->userdata('email')])->row_Array();
    }
    public function hapusLaporan($id)
    {
        $this->db->delete('laporan', ['id' => $id]);
    }
    public function cekEmail()
    {
        return $this->db->get_where('checkout', ['email' => $this->session->userdata('email')])->row_array();
    }
}