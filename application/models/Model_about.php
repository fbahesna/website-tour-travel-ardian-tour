<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_about extends CI_Model
{
    public function tampilAbout()
    {
        return $this->db->get('about');
    }
    public function editAbout()
    {
        $data = [
            'judul' => $this->input->post('judul'),
            'about' => $this->input->post('about')
        ];
        $this->db->set('about', $data);
        $this->db->update('about');
    }
    public function cekEmail()
    {
        return $this->db->get('pesan')->result_array();
    }
    public function hapusPesan($id)
    {

        $this->db->delete('pesan', ['id' => $id]);
    }
    public function sendEmail()
    {
        $data = [
            "email" => $this->input->post('email', true),
            "subject" => $this->input->post('subject', true),
            "pesan" => $this->input->post('pesan', true),
        ];
        $this->db->insert('pesan', $data);
    }
}
