<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_promo extends CI_Model

{
    public function getAllPromo()
    {
        return $this->db->get('promo');
    }
    public function getById($id)
    {
        return $this->db->get_where('promo', ['id' => $id])->row_array();
    }

    public function tambahPromo()
    {
        $img = $this->upload->data();
        $gambar = $img['file_name'];
        $data = [
            "nama_promo" => $this->input->post('nama_promo', true),
            "bulan_promo" => $this->input->post('bulan_promo', true),
            "harga_promo" => $this->input->post('harga_promo', true),
            "potongan_promo" => $this->input->post('potongan_promo', true),
            "pesan_promo" => $this->input->post('pesan_promo', true),
            "durasi_promo" => $this->input->post('durasi_promo', true),
            "image" => $gambar
        ];
        $this->db->insert('promo', $data);
    }
    public function editPromo()
    {
        $data = [
            "nama_promo" => $this->input->post('nama_promo'),
            "bulan_promo" => $this->input->post('bulan_promo'),
            "harga_promo" => $this->input->post('harga_promo'),
            "potongan_promo" => $this->input->post('potongan_promo'),
            "pesan_promo" => $this->input->post('pesan_promo'),
            "durasi_promo" => $this->input->post('durasi_promo'),

        ];
        $upload_image = $_FILES['image']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'jpg|gif|png';
            $config['max_size'] = '50000';
            $config['upload_path'] = './uploadPromo/';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image') == false) {
                echo $this->upload->display_errors();
            } else {
                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            }
        }
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('promo', $data);
    }
    public function hapusPromo($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('promo');
    }
    // Insert promo data from user form
    public function ambilPromo()
    {
        $data = [
            "nama" => $this->input->post('nama'),
            "email" => $this->input->post('email'),
            "notelepon" => $this->input->post('notelepon'),
            "alamat" => $this->input->post('alamat'),
            "potongan_promo" => $this->input->post('potongan_promo'),
            "tujuan_promo" => $this->input->post('tujuan_promo'),
            "date_created" => date("d-m-Y"),
            "harga_normal" => $this->input->post('harga_normal'),
            "durasi_promo" => $this->input->post('durasi_promo'),
            "qty" => $this->input->post('qty'),
            "total_bayar" => $this->input->post('qty') * $this->input->post('harga_normal') - $this->input->post('potongan_promo')
        ];
        $cekEmailPromo = $this->db->get_where('checkout_promo', ['email' => $this->session->userdata('email')])->row_array();
        if ($cekEmailPromo == false) {
            $this->db->insert('checkout_promo', $data);
            // $this->__kirimEmail(); 
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h2>Promo berhasil di ambil</h2></div>');
            redirect('Promo/showPromo');
        } else {
            $this->load->model('M_user');
            $data['user'] = $this->M_user->getUser()->row_array();
            $this->load->view('templates/user/user_header', $data);
            $this->load->view('pesan/blockedOrder');
        }
    }
    private function _kirimEmail()
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'ardiantour46@gmail.com',
            'smtp_pass' => 'Ardian1234',
            'smtp_port' => 465,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];
        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('ardiantour46@gmail.com', 'Ardian Tour');
        $this->email->to($this->input->post('email'));
        $this->email->subject('Konfirmasi Pemesanan Paket Wisata');
        $message = 'Pengambilan promo telah diproses,admin akan segera menghubungi dan mengkonfirmasi pesanan promo.Terima kasih telah mempercayai kami sebagai penyedia layanan jasa tour wisata.';
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }
    public function cancelPromo()
    {
        $this->db->delete('checkout_promo', ['email' => $this->session->userdata('email')]);
    }
}
