<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_user extends CI_Model
{



    function getAllPaket($number, $offset)
    {
        return $this->db->get('tbl_paket', $number, $offset);
    }
    function getAll()
    {
        return $this->db->get('tbl_paket');
    }

    function getUser()
    {
        return $this->db->get('user', ['email' => $this->session->userdata('email')]);
    }
}
