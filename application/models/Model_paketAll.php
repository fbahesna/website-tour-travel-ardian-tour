<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_paketAll extends CI_Model

{
    public function tampilDataPagination($limit, $start)
    {
        return $this->db->get('tbl_paket', $limit, $start);
    }
    public function tampilData()
    {
        return $this->db->get('tbl_paket');
    }
}
