<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_menu extends CI_Model
{

    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `menu_user`.`menu`
                FROM `user_sub_menu` JOIN `menu_user`
                ON `user_sub_menu`.`menu_id` = `menu_user`.`id`
       ";
        return $this->db->query($query)->result_array();
    }

    public function hapusMenu($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('menu_user');
    }
    public function hapusSubMenu($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_sub_menu');
    }
}
