<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_Transaksi extends CI_Model
{
    public function tampilSemua()
    {
        return $this->db->get('checkout')->result_array();
    }
    public function tampilById($id)
    {
        return $this->db->get_where('checkout', ['id' => $id])->row_array();
    }
    public function tampilPromoById($id)
    {
        return $this->db->get_where('promo', ['id' => $id])->row_array();
    }
    public function hapusCheckout($id)
    {
        $this->db->delete('checkout', ['id' => $id]);
    }
    public function editCheckout($id)
    {
        $data = [
            "id" => $this->input->post($id),
            "tujuan_wisata" => $this->input->post('tujuan_wisata', true),
            "namalengkap" => $this->input->post('namalengkap', true),
            "email" => $this->input->post('email', true),
            "notelepon" => $this->input->post('notelepon', true),
            "alamat" => $this->input->post('alamat', true),
            "date" => $this->input->post('date', true),
            "durasi" => $this->input->post('durasi', true),
            "qty" => $this->input->post('qty', true)
        ];
        $this->db->set($data);
        $this->db->update('checkout', ['id' => $this->input->post($id)]);
    }

    //cek pesanan setelah user pesan dan tampil by sesi user 
    public function cekEmail()
    {
        // "SELECT `checkout`.*,`user`.`email`
        //           FROM `checkout` JOIN `user`
        //           ON `checkout`.`email` = `user`.`email`
        // ";
        // return $this->db->query($query)->row_array();
        return $this->db->get_where('checkout', ['email' => $this->session->userdata('email')])->row_array();
    }
    public function prosesPesanan($id)
    {
        return  $this->db->get_where('checkout', ['id' => $id])->row_array();
    }
    public function proses()
    {
        $data = [
            'nama' => $this->input->post('namalengkap'),
            'email' => $this->input->post('email'),
            'notelp' => $this->input->post('notelepon'),
            'alamat' => $this->input->post('alamat'),
            'tujuan' => $this->input->post('tujuan'),
            'durasi' => $this->input->post('durasi'),
            'keberangkatan' => $this->input->post('keberangkatan'),
            'date_created' => $this->input->post('date_created'),
            'peserta' => $this->input->post('peserta'),
            'total' => $this->input->post('total_bayar')
        ];
        $d = $this->db->insert('laporan', $data);
    }
    public function laporanPesanan()
    {
        return $this->db->get('laporan')->result_array();
    }
}
