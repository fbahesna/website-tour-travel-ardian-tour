<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_tes extends CI_Model
{
    function tampil()
    {
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->db->select('id, durasi, qty, SUM(durasi*qty) as total');
        $this->db->group_by('durasi');
        $this->db->order_by('total', 'desc');
        $hasil = $this->db->get_where('checkout', ['email' => $this->session->userdata('email')]);
        return $hasil;
    }
}
