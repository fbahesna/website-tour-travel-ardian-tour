<?php defined('BASEPATH') or exit('No direct script access allowed');

class Model_voucher extends CI_Model
{


    public function getVoucher()
    {
        $lama = 3; // lama data adalah 3 hari

        // proses penghapusan data

        "DELETE FROM voucher
          WHERE DATEDIFF(CURDATE(), tanggal) > $lama";

        return $this->db->get('voucher')->result_array();
    }

    public function getNoVoucher()
    {
        $q = $this->db->query("SELECT MAX(RIGHT(id,4)) AS kd_max FROM voucher WHERE DATE(tanggal)=CURDATE()");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int) $k->kd_max) + 1;
                $kd = sprintf("%04s", $tmp);
            }
        } else {
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy') . $kd;
    }
    public function saveVoucher()
    {
        $data = [
            'id' => $this->input->post('id'),
            'diskon' => $this->input->post('diskon')
        ];
        $this->form_validation->set_rules('diskon', 'Diskon', 'required|trim');
        $this->form_validation->set_rules('id', 'Id', 'required|trim');
        if ($this->form_validation->run()) {
            $this->db->insert('voucher', $data);
        } else {
            $this->session->set_flashdata('voucher', '<h5 class="text-danger">Voucher gagal dibuat,data harus terisi semua!</h5>');
        }
    }
    public function hapus($id)
    {
        $this->db->delete('voucher', ['id' => $id]);
    }
    // public function editVoucher($id)
    // {
    //     $data = [
    //         'diskon' => $this->input->post('diskon'),
    //         'id' => $this->input->post('id')
    //     ];
    //     $this->form_validation->set_rules('diskon', 'Diskon', 'required|trim');
    //     $this->form_validation->set_rules('id', 'Id', 'required|trim');

    //     if ($this->form_validation->run()) {
    //         $this->db->set($data);
    //         $this->db->insert('voucher', ['id' => $id]);
    //     } else {
    //         $this->session->set_flashdata('voucher', '<h5 class="text-danger">Voucher gagal dibuat/diubah,data harus terisi semua!</h5>');
    //     }
    // }
    public function expired()
    {
        $date_now = date("Y-m-d");
        $date_start = $this->input->post('tanggal');

        $jangka = strtotime('+' . $this->input->post(), strtotime($date_start));
        $expired_date = date("Y-m-d", $jangka);
        if ($date_now >= $expired_date) {
            echo "Tidak berlaku mamank!!!";
        } else {
            $this->db->delete('voucher', ['tanggal' => $jangka]);
        }
    }
}
