<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_crud extends CI_Model
{
    function tampil_data()
    {
        return $this->db->get('tbl_paket');
    }
    function tambahDataPaket()
    {
        $img = $this->upload->data();
        $gambar = $img['file_name'];
        $data = [
            "paket_tour" => $this->input->post('paket_tour', true),
            "tujuan" => $this->input->post('tujuan', true),
            "keterangan" => $this->input->post('keterangan', true),
            "image" => $gambar,
            "one_day" => $this->input->post('one_day', true),
            "n2D1N" => $this->input->post('n2D1N'),
            "n3D2N" => $this->input->post('n3D2N'),
            "n4D3N" => $this->input->post('n4D3N')
        ];
        $this->db->insert('tbl_paket', $data);
    }
    public function tampilById($id)
    {
        return $this->db->get_where('tbl_paket', ['id' => $id])->row_array();
    }
    public function hapusPaket($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_paket');
    }
    function editDataPaket()
    {
        $data = [
            "id" => $this->input->post('id', true),
            "paket_tour" => $this->input->post('paket_tour', true),
            "tujuan" => $this->input->post('tujuan', true),
            "keterangan" => $this->input->post('keterangan', true),
            "one_day" => $this->input->post('one_day', true),
            "n2D1N" => $this->input->post('n2D1N', true),
            "n3D2N" => $this->input->post('n3D2N', true),
            "n4D3N" => $this->input->post('n4D3N', true),
        ];
        $upload_image = $_FILES['image']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpg|gif|png';
            $config['max_size'] = '50000';
            $config['upload_path'] = './upload/';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image') == false) {
                echo $this->upload->display_errors();
            } else {
                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            }
        }
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('tbl_paket', $data);
    }
    public function hapusCheckoutPromo($id)
    {
        $this->db->delete('checkout_promo', ['id' => $id]);
    }
}
