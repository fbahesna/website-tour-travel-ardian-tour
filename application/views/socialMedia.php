<?php
function get_CURL($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return json_decode($result, true);
}
$clientId = '	6f14e59e1fdc47e683a66cb0d748068d';
$tokenId = '15017633637.6f14e59.44ee4e4fddec4773a63f4009fb84f49c';
$result = get_cURL('https://api.instagram.com/v1/users/self?access_token=15017633637.6f14e59.44ee4e4fddec4773a63f4009fb84f49c');

$usernameIG = $result['data']['username'];
$profilePicIG = $result['data']['profile_picture'];
$media = $result['data']['counts']['media'];
$followers = $result['data']['counts']['followed_by'];
$follows = $result['data']['counts']['follows'];

// IG image post
$result = get_cURL('https://api.instagram.com/v1/users/self/media/recent?access_token=15017633637.6f14e59.44ee4e4fddec4773a63f4009fb84f49c&counts=5');

$post = [];
foreach ($result['data'] as $p) {
    $post[] = $p['images']['thumbnail']['url'];
}
?>
<section class="recent-blog-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col text-center">
                <h1 class="mb-10">Social Media</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <i class="fab fa-instagram fa-7x ig-icon"></i>
            </div>
            <div class="col-md-5">
                <i class="fab fa-fw fa-whatsapp fa-7x wa-icon"></i>
            </div>
        </div>
        <div class="container-ig col-lg-6">
            <div class="row ig-row">
                <div class="col-lg-2">
                    <h5 class="usrname"><?= $usernameIG; ?></h5>
                    <img class="dp-IG" src="<?= $profilePicIG; ?>" width="100" style="border-radius:50px;">
                </div>
                <div class="row" style="margin-top:40px; margin-left:100px;">
                    <div class="col-lg">
                        <h4 style="margin-left:28px;"><?= $media; ?></h4>
                        <p> Postingan</p>
                    </div>
                    <div class="col-lg">
                        <h4 style="margin-left:28px;"><?= $followers; ?></h4>
                        <p> Pengikut</p>
                    </div>
                    <div class="col-lg">
                        <h4 style="margin-left:28px;"><?= $follows; ?></h4>
                        <p> Mengikuti</p>
                    </div>
                </div>
            </div>
            <div class="row follow-button">
                <div class="col">
                    <a href="https://www.instagram.com/ardian_tour/" class="btn btn-primary btn-sm btn-block">Ikuti</a>
                </div>
            </div>
            <div class="row mt-3 pb-3">
                <div class="col">
                    <?php foreach ($post as $p) : ?>
                        <div class="ig-thumbnail">
                            <img src="<?= $p; ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <!-- Klik WA -->
                <a href="https://wa.me/6281227426947?text=Silahkan%20mengajukan%20pertanyaan%20mengenai%20paket%20wisata%20yang%20disediakan%20oleh%20Ardian%20Tour" class="btn btn-success btn-lg wa">Kirim Pesan</a>
                <!-- END Klik WA -->
            </div>
        </div>
</section>