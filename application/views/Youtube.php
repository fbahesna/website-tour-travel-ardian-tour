<?php
// $apiKey = 'AIzaSyDOiap1Y9LTSnDLdYA81oHoD9JYO82Qd2M';
function get_Tube($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return json_decode($result, true);
}
$result = get_Tube('https://www.googleapis.com/youtube/v3/channels?part=snippet,statistics&id=UCcKyBmaIsrH-cFERXlAk0KA&key=AIzaSyDOiap1Y9LTSnDLdYA81oHoD9JYO82Qd2M');

$PublishDate = $result['items'][0]['snippet']['publishedAt'];

// Latest Video

$result = get_Tube('https://www.googleapis.com/youtube/v3/search?key=AIzaSyDOiap1Y9LTSnDLdYA81oHoD9JYO82Qd2M&channelId=UCcKyBmaIsrH-cFERXlAk0KA&maxResults=3&order=date&part=snippet');

$latestVideoId = $result['items'][0]['id']['videoId'];
$judulVideo = $result['items'][0]['snippet']['title'];
$descVideo = $result['items'][0]['snippet']['description'];




// Pengulangan Judul Video
// Pengulangan Desc Video
// Pengulangan Video
$post = [];
foreach ($result['items'] as $V) {
    $post[] = $V['id']['videoId'];
}
$judul = [];
foreach ($result['items'] as $m) {
    $judul[] = $m['snippet']['title'];
}
$desc = [];
foreach ($result['items'] as $n) {
    $desc[] = $n['snippet']['description'];
}

?>

<link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/social.css">
<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<?php $var = 'testtesttest'; ?>
<section class="recent-blog-area-youtube section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10">Ardian Tour</h1>
                    <i class="fab fa-youtube fa-7x youtube-icon"></i>
                </div>
            </div>
        </div>
        <div class="row mt-3 pb-3">
            <div class="active-recent-blog-carusel">
                <?php foreach ($post as $vid) : ?>
                    <div class="single-recent-blog-post item">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $vid; ?>?rel=0" allowfullscreen></iframe>
                        </div>

                    </div>
                <?php endforeach; ?>


            </div>
        </div>
    </div>
</section>