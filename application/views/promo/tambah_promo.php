<div class="container">
    <h2><?= $title; ?></h2>
</div>
<div class="container">
    <div class="container-fluid col-lg-5 float-left">

        <div class="row-mt-5">
            <div class="row-md-7">
                <?= form_open_multipart('promo/tambahPromo'); ?>

                <?= validation_errors(); ?>
                <div class="form-group">
                    <label for="nama_promo">Nama Promo</label>
                    <input type="text" class="form-control" id="nama_promo" name="nama_promo">
                </div>
                <div class="form-group">
                    <label for="bulan_promo">Bulan Promo</label>
                    <input type="text" class="form-control" id="bulan_promo" name="bulan_promo">
                </div>
                <div class="form-group">
                    <label for="harga_promo">Harga</label>
                    <input type="text" class="form-control" id="harga_promo" name="harga_promo">
                </div>
                <div class="form-group">
                    <label for="potongan_promo">Potongan Harga</label>
                    <input type="text" class="form-control" id="potongan_promo" name="potongan_promo">
                </div>
                <div class="form-group">
                    <label for="pesan_promo">Pesan Promo</label>
                    <input type="text" class="form-control" id="pesan_promo" name="pesan_promo">
                </div>
                <div class="form-group">
                    <label for="durasi_promo">Durasi</label>
                    <input type="text" class="form-control" id="durasi_promo" name="durasi_promo">
                </div>
                <div class="form-group">
                    <label for="image">Masukkan Gambar</label>
                    <input type="file" name="image" class="form-control-file"> <br />
                    <input type="submit" class="btn btn-primary" value="Tambah Promo" />
                    <?= form_close() ?>
                </div>
            </div>
        </div>

    </div>
</div>
</div>