<!DOCTYPE html>
<html>

<head>
  <title><?= $title; ?></title>
</head>

<body>
  <div class="container-fluid ">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <a class="btn btn-primary" style="margin-left:540px; margin-bottom:20px;" href="<?= base_url('promo/tambahPromo'); ?>">Tambah Data</a>

    <table class="table table-bordered table-dark m-auto align-center table-responsive" style="border-radius:10px; ">
      <thead>
        <?= $this->session->flashdata('message'); ?>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Promo</th>
          <th scope="col">Bulan Promo</th>
          <th scope="col">Harga Promo</th>
          <th scope="col">Potongan Promo</th>
          <th scope="col">Pesan Promo</th>
          <th scope="col">Gambar</th>

        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        <?php foreach ($promo as $u) :
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $u->nama_promo ?></td>
            <td><?php echo $u->bulan_promo ?></td>
            <td>Rp.<?php echo number_format($u->harga_promo, 0, '', '.'); ?></td>
            <td>Rp.<?php echo number_format($u->potongan_promo, 0, '', '.'); ?></td>
            <td><?php echo $u->pesan_promo ?></td>


            <td><img src="<?php echo base_url('UploadPromo/' . $u->image) ?>" width="250" style="border-radius:20px;" height="150"></td>

            <td> <a class="btn btn-primary " href="<?= base_url(); ?>promo/editPromo/<?= $u->id ?>">Edit</a> <br> <br>
              <a class="btn btn-danger " onclick="return confirm('PERHATIAN!!! Anda yakin ingin menghapus data ini?');" href="<?= base_url(); ?>promo/hapusPromo/<?= $u->id ?>">Hapus</a> </td>

          </tr>
          <?php $i++; ?>
        <?php endforeach; ?>
  </div>
  </tbody>
  </table>

</body>

</html>