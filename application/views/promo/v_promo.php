    <!-- link css -->
    <link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/promo.css">

    <body>
        <!-- Dashboard Promo  -->
        <div class="container-con">
            <div class="row col-lg">
                <img class="img-con" src="<?= base_url('upload/sale.jpg'); ?>">
            </div>
        </div>
        <!-- End Dashboard Promo -->
        <div class="container">
            <?php foreach ($promo as $p) : ?>
                <div class="row">
                    <div class="col">
                        <img class="img-fluid" alt="reponsive-image" src="<?= base_url('uploadPromo/' . $p->image); ?>">
                    </div>
                    <div class="col pesan_promo">
                        <?php if ($this->session->userdata('email')) : ?>
                            <a href="<?= base_url() ?>promo/ambilPromo/<?= $p->id; ?>">
                                <h2><?= $p->pesan_promo; ?></h2>
                            </a>
                        <?php elseif (!$this->session->userdata('email')) : ?>
                            <a href="#" data-toggle="modal" data-target="#exampleModal">
                                <h2><?= $p->pesan_promo; ?></h2>
                            </a>

                        <?php endif; ?>
                    </div>
                    <?php if ($this->session->userdata('email')) : ?>
                        <div class="col">
                            <a href="<?= base_url() ?>promo/ambilPromo/<?= $p->id; ?>" class="btn btn-success  btn-ambil">Ambil Promo</a>
                        </div>
                    <?php elseif (!$this->session->userdata('email')) : ?>
                        <div class="col">
                            <button type="button" class="btn btn-success btn-ambil" data-toggle="modal" data-target="#exampleModal">
                                Ambil Promo
                            </button>
                        </div>
                    <?php endif; ?>
                </div>
                <hr />
            <?php endforeach; ?>
        </div>
    </body>
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" style="background-color:darkorange;">
                    <h4>Anda belum Login,silahkan Login terlebih dahulu.</h4>
                </div>
                <div class="modal-footer" style="background-color:darkslategrey;">
                    <a href="<?= base_url('auth/login'); ?>" type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Masuk</a>
                    <a href="<?= base_url('auth/registration'); ?>" type="button" class="btn btn-success btn-sm">Mendaftar</a>
                </div>
            </div>
        </div>
    </div>