<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<h1 class="h3 mb-4 text-gray-800"><?= $title2; ?></h1>

<div class="div row">
    <div class="col-lg">
        <?php if ($sesiPromo) : ?>
            <?= $this->session->flashdata('message'); ?>
        <?php endif; ?>
        <!-- Cek user pesan/belum -->
        <?php if (!$sesiPromo) : ?>
            <div class="col-md-5" style="margin-left:400px;">
                <h3 class="text-danger">Belum ada promo yang anda ambil!</h3>
            </div>
        <?php endif; ?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Nomor Telp</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Potongan</th>
                    <th scope="col">Tujuan</th>
                    <th scope="col">Durasi</th>
                    <th scope="col">Peserta</th>
                    <th scope="col">Harga Normal</th>
                    <th scope="col">Sub Total Pembayaran</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <tr>
                    <td scope="row"><?= $i; ?></td>
                    <td><?= $sesiPromo['nama'];   ?></td>
                    <td><?= $sesiPromo['email'];   ?></td>
                    <td><?= $sesiPromo['notelepon'];   ?></td>
                    <td><?= $sesiPromo['alamat'];   ?></td>
                    <td>Rp.<?= number_format($sesiPromo['potongan_promo'], 0, ',', '.');   ?></td>
                    <td><?= $sesiPromo['tujuan_promo'];   ?></td>
                    <td><?= $sesiPromo['durasi_promo'];   ?></td>
                    <td><?= $sesiPromo['qty'];   ?></td>
                    <td>Rp.<?= number_format($sesiPromo['harga_normal'], 0, ',', '.');   ?></td>
                    <td>Rp<?php
                            $a = array($sesiPromo['harga_normal'] * $sesiPromo['qty'] - $sesiPromo['potongan_promo']);
                            echo " Total = " . number_format(array_sum($a), 0, ',', '.') . "\n";
                            ?></td>
                    <td>
                    </td>
                </tr>
                <?php $i++; ?>

            </tbody>
            <a title="Batalkan Pesanan!!!" href="<?= base_url() ?>promo/cancelPromo" class="fas fa-fw fa-window-close fa-2x" onclick="return confirm('Anda yakin ingin membatalkan pesanan?')"></a>
        </table>