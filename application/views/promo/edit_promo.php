<div class="container-fluid col-lg-3">

    <div class="row-mt-3">
        <div class="row-md-6">
            <?= form_open_multipart('promo/simpanEditPromo') ?>
            <input type="hidden" name="id" value="<?= $promo['id']; ?>">
            <?= validation_errors(); ?>
            <div class="form-group">
                <label for="nama_promo">Nama Promo</label>
                <input type="text" class="form-control" id="nama_promo" name="nama_promo" value="<?= $promo['nama_promo']; ?>">
            </div>
            <div class="form-group">
                <label for="bulan_promo">Bulan Promo</label>
                <input type="text" class="form-control" id="bulan_promo" name="bulan_promo" value="<?= $promo['bulan_promo']; ?>">
            </div>
            <div class="form-group">
                <label for="harga_promo">Harga Promo</label>
                <input type="text" class="form-control" id="harga_promo" name="harga_promo" value="<?= $promo['harga_promo']; ?>">
            </div>
            <div class="form-group">
                <label for="potongan_promo">potongan Promo</label>
                <input type="text" class="form-control" id="potongan_promo" name="potongan_promo" value="<?= $promo['potongan_promo']; ?>">
            </div>
            <div class="form-group">
                <label for="pesan_promo">Pesan Promo</label>
                <input type="text" class="form-control" id="pesan_promo" name="pesan_promo" value="<?= $promo['pesan_promo']; ?>">
            </div>
            <div class="form-group">
                <img src="<?= base_url('uploadPromo/') . $promo['image']; ?>" class="img-thumbnail" width="200" height="200">
                <label for="image">Edit Gambar</label>
                <input type="file" name="image" class="form-control-file"> <br />
                <input type="submit" class="btn btn-primary" value="Edit Promo" />
                <?= form_close() ?>
            </div>
        </div>
    </div>

</div>