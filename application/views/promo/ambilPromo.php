<div class="container mb-auto" style="margin-top:90px;">
    <a class="btn btn-danger" onclick="return confirm('Pesanan akan dibatalkan! Anda yakin?');" href="<?= base_url('user') ?> ">Batalkan Pemesanan</a>
</div>
<div class="container col-md-5 align-left border" style="background-color:whitesmoke;">
    <h3><?= $title; ?>:<br /><br />
        <h2><?= $promo['nama_promo']; ?></h2>
    </h3>
    <h1 class="h3 mb-4 text-gray-800"></h1>
 <!-- <form method="post" action="<?= base_url(); ?>Promo/ambilPromo/<?= $promo['id']; ?>"> -->
    <?= form_open('Promo/ambilPromo/' . $promo['id']); ?>

    <!-- <?= $this->session->set_flashdata('message'); ?> -->

    <div class="form-group">
        <label for="nama">
            <h4>Nama Lengkap</h4>
        </label>
        <input type="text" name="nama" class="form-control" id="nama">
    </div>
    <div class="form-group">
        <label for="email">
            <h4>Email</h4>
        </label>
        <input type="text" name="email" class="form-control" id="email" value="<?= $user['email']; ?>" readonly>
    </div>
    <div class="form-group">
        <label for="notelepon">
            <h4>No Telepon (Yang Bisa Dihubungi)</h4>
        </label>
        <input type="text" name="notelepon" class="form-control" id="notelepon">
    </div>
    <div class="form-group">
        <label for="alamat">
            <h4>Alamat</h4>
        </label>
        <textarea type="text" name="alamat" class="form-control" id="alamat" rows="3"></textarea>
    </div>
    <!-- Type Hidden -->
    <!-- <input type="hidden" value="<?= $promo['potongan_promo']; ?>" name="promo" id="promo"> -->
    <!-- END -->
    <div class="form-group">
        <label for="tujuan_promo">
            <h4>Tujuan Wisata</h4>
        </label>
        <input type="text" name="tujuan_promo" class="form-control" id="tujuan_promo" value="<?= $promo['nama_promo']; ?>" readonly>
    </div>
    <div class="form-group" style="margin-bottom:80px;">
        <div class="form-group">
            <label for="potongan_promo">
                <h4>Potongan Promo</h4>
            </label>
            <input type="text" name="potongan_promo" class="form-control" id="potongan_promo" value="<?= $promo['potongan_promo']; ?>" readonly>
        </div>
        <div class="form-group">
            <label for="harga_normal">
                <h4>Harga Normal</h4>
            </label>
            <input type="text" name="harga_normal" class="form-control" id="harga_normal" value="<?= $promo['harga_promo']; ?>" readonly>
        </div>
        <div class="form-group">
            <label for="durasi_promo">
                <h4>Durasi Promo</h4>
            </label>
            <input type="text" name="durasi_promo" class="form-control" id="durasi_promo" value="<?= $promo['durasi_promo']; ?>" readonly>
        </div>

        <div class="row col-lg-5">
            <label for="qty">
                <h4>Peserta</h4>
            </label>
            <input class="form-control" type="text" id="qty" name="qty" placeholder="Banyaknya peserta tour">
            <small class="form-text text-muted">Banyaknya peserta termasuk anda</small>
        </div>
        <input type="submit" class="btn btn-success float-right mt-4" value="Pesan" onclick="return confirm('Dengan ini saya menyetujui syarat&ketentuan  dari Ardian Tour');" />
    </div>
</div>
<?= form_close(); ?>
<!-- </form> -->

<br /><br /><br />