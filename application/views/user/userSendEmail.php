<div class="container col-md-5" style="margin-top:200px; margin-bottom:200px; background-color:whitesmoke;">
    <div class="row">
        <div class="col text-center">
            <h3><?= $title;     ?></h3>
        </div>
    </div>
    <form action="<?= base_url('about/sendEmail'); ?>" method="post">
        <div class="form-group">
            <label for="email">
                <h5>Masukkan Email</h5>
            </label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?= $user['email']; ?>">
        </div>
        <div class="form-group">
            <label for="subject">
                <h5>Masukkan Subject</h5>
            </label>
            <input type="text" class="form-control" id="subject" name="subject" aria-describedby="emailHelp">
        </div>
        <div class="form-group">
            <label for="pesan">
                <h5>Masukkan Pesan</h5>
            </label>
            <textarea class="form-control" id="pesan" name="pesan" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>