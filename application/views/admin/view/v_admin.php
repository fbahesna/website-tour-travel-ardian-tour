<!DOCTYPE html>
<html>

<head>
  <title><?= $title; ?></title>
</head>

<body>
  <div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800" style="margin-left:40%;"><?= $title; ?></h1>
    <table class="table table-bordered table-dark m-auto align-center" style="border-radius:40px;">
      <thead>
        <?= $this->session->flashdata('message'); ?>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Paket</th>
          <th scope="col">Tujuan</th>
          <th scope="col">One Day</th>
          <th scope="col">2D1N</th>
          <th scope="col">3D2N</th>
          <th scope="col">4D3N</th>
          <th scope="col">Gambar</th>
          <th scope="col">Keterangan</th>
          <th scope="col"><i style="margin-left:13px;" class="fas fa-fw fa-edit"></i></th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        <?php foreach ($tbl_paket as $u) :
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $u->paket_tour ?></td>
            <td><?php echo $u->tujuan ?></td>
            <td>Rp<?php echo number_format($u->one_day), ',', '.' ?></td>
            <td>Rp<?php echo number_format($u->n2D1N), ',', '.' ?></td>
            <td>Rp<?php echo number_format($u->n3D2N), ',', '.' ?></td>
            <td>Rp<?php echo number_format($u->n4D3N), ',', '.' ?></td>
            <td><img src="<?php echo base_url('upload/' . $u->image) ?>" width="250" height="150" style="border-radius:20px;"></td>

            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?= $u->id; ?>">
                <i class="fas fa-fw fa-eye"></i>
              </button></td>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal<?= $u->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Keterangan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?= $u->keterangan ?>
                  </div>

                </div>
              </div>
            </div>
            <td> <a class="badge badge-info active" href="<?= base_url(); ?>admin/edit/<?= $u->id ?>">Edit</a> <br> <br>
              <a class="badge badge-danger active" onclick="return confirm('PERHATIAN!!! Anda yakin ingin menghapus data ini?');" href="<?= base_url(); ?>admin/hapus/<?= $u->id ?>">Hapus</a> </td>
          </tr>
          <?php $i++; ?>
        <?php endforeach; ?>
  </div>
  </tbody>
  </table>

</body>
<!-- Button trigger modal -->




</html>