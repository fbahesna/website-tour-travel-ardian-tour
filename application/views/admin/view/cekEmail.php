<div class="container">
    <h2 style="margin-left:420px;" class="text-info"><?= $title; ?></h2>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">Pesan</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1;
            foreach ($pesan as $P) : ?>
                <tr>
                    <th scope="col"><?= $i++; ?></th>
                    <th scope="col"><?= $P['email']; ?></th>
                    <th scope="col"><?= $P['subject']; ?></th>
                    <th scope="col"><?= $P['pesan']; ?></th>
                    <th><a type="button" class="btn btn-danger" onclick="return confirm('Yakin Akan Dihapus?');" href="<?= base_url() ?>about/hapusPesanMasuk/<?= $P['id']; ?>">Hapus</a>
                        <a type="button" class="btn btn-success" href="https://mail.google.com/mail/?view=cm&fs=1&to=" target="blank">Balas</a></th>
                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
</div>