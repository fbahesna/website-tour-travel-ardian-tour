<div class="container">
    <div class="col-md-5">
        <?php if (!$cekInputVoucher) : ?>
            <?= $this->session->flashdata('flash2'); ?>
        <?php elseif ($cekInputVoucher) : ?>
            <?= $this->session->flashdata('voucher'); ?>
        <?php endif; ?>

    </div>
    <div class="row">
        <div class="col">

            <body>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
                    Buat Voucher
                </button>
                <?= $this->session->flashdata('message'); ?>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Buat Voucher</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url() . 'Voucher/saveVoucher' ?>" method="post">
                                    <input type="text" name="id" id="id" value="<?= $voucher; ?>">
                                    <input type="text" name="diskon" id="diskon " placeholder="masukkan potongan">
                                    <button class="btn btn-success" type="submit">Simpan</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </body>
        </div>
        <div class="col">
            <div id="content">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Kode Voucher</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Diskon</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($getVoucher as $v) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $v['id']; ?></td>
                            <td><?= $v['tanggal']; ?></td>
                            <td>Rp.<?= number_format($v['diskon'], 0, ',', '.'); ?></td>
                            <td>
                                <a onclick="return confirm('Yakin ingin menghapus Voucher ini?');" href="<?= base_url('voucher/hapusVoucher/'); ?>/<?= $v['id']; ?>">Hapus</a>
                            </td>
                        </tr>
                        <?= $i++; ?>
                    <?php endforeach; ?>

                </tbody>
            </table>





            <!-- <script type="text/javascript">
                getData();

                function getData() {
                    $.ajax({
                        type: 'POST',
                        url: < ? = base_url().
                        "voucher/getData" ? > ,
                        dataType : 'JSON',
                        success: function(data) {
                            console.log(data);
                        }
                    })
                }
            </script> -->
        </div>
    </div>
</div>