<div class="container-fluid mt-5">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="div row">
        <div class="col-lg">
            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">No Telepon</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Tujuan</th>
                        <th scope="col">Durasi</th>
                        <?php if ($voucher) : ?>
                            <th scope="col">Voucher</th>
                        <?php endif; ?>
                        <th scope="col">Keberangkatan</th>
                        <th scope="col">Tanggal Pemesanan</th>
                        <th scope="col">Peserta</th>
                        <th scope="col">Sub Total</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($laporanPesanan as $laporan) : ?>
                        <tr>
                            <td scope="row"><?= $i; ?></td>
                            <td><?= $laporan['nama'];   ?></td>
                            <td><?= $laporan['email'];   ?></td>
                            <td><?= $laporan['notelp'];   ?></td>
                            <td><?= $laporan['alamat'];   ?></td>
                            <td><?= $laporan['tujuan'];   ?></td>
                            <td>Rp.<?= number_format($laporan['durasi'], 0, ',', '.'); ?></td>
                            <?php if ($voucher) : ?>
                                <td><?= $laporan['voucher'];   ?></td>
                            <?php endif; ?>
                            <td><?= $laporan['keberangkatan'];   ?></td>
                            <td><?= $laporan['date_created'];   ?></td>
                            <td><?= $laporan['peserta'];   ?></td>
                            <td>Rp.<?= number_format($laporan['total'], 0, ',', '.'); ?>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>