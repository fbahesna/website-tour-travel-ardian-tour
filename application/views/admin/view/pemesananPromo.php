<div class="container-fluid mt-5">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="div row">
        <div class="col-lg">
            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col"> No Telp</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Potongan Promo</th>
                        <th scope="col">Tujuan Promo</th>
                        <th scope="col">Durasi Promo</th>
                        <th scope="col">Peserta</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Total Bayar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($pemesananPromo as $pm) : ?>
                        <tr>
                            <td scope="row"><?= $i; ?></td>
                            <td><?= $pm['nama'];   ?></td>
                            <td><?= $pm['email']; ?></td>
                            <td><?= $pm['notelepon'];   ?></td>
                            <td><?= $pm['alamat'];   ?></td>
                            <td>Rp.<?= number_format($pm['potongan_promo'], 0, ',', '.');   ?></td>
                            <td><?= $pm['tujuan_promo'];   ?></td>
                            <td><?= $pm['durasi_promo'];   ?></td>
                            <td><?= $pm['qty'];   ?></td>
                            <td>Rp.<?= number_format($pm['harga_normal'], 0, ',', '.');   ?></td>
                            <td>Rp.<?= number_format($pm['total_bayar'], 0, ',', '.'); ?>
                            <td>
                                <a href="<?= base_url(); ?>admin/hapusCheckoutPromo/<?= $pm['id']; ?>" class="badge badge-danger" onclick="return confirm('PERHATIAN!!! Anda yakin ingin menghapus data ini?');">Hapus </a>

                                <a href="<?= base_url(); ?>transaksi/cetakPromo/<?= $pm['id']; ?>" class="badge badge-success">Cetak</a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>

                </tbody>
            </table>