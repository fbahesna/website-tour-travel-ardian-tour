<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="container-fluid">
        <?= form_open_multipart('admin/simpanEdit'); ?>
        <input type="hidden" name="id" value="<?= $tbl_paket['id']; ?>">
        <div class="form-group">
            <label for="paket_tour">Paket Tour</label>
            <input type="text" name="paket_tour" class="form-control" id="paket_tour" value="<?= $tbl_paket['paket_tour']; ?>">
        </div>
        <div class="form-group">
            <label for="tujuan">Tujuan</label>
            <input type="text" name="tujuan" class="form-control" id="tujuan" value="<?= $tbl_paket['tujuan']; ?>">
        </div>
        <div class="border">
            <label>Harga</label>
            <div class="row">
                <div class="col">
                    <label for="one_day">One Day</label>
                    <input style="margin-left:20px;" type="text" name="one_day" id="one_day" value="<?= $tbl_paket['one_day']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n2D1N">2D1N</label>
                    <input style="margin-left:40px;" type="text" name="n2D1N" id="n2D1N" value="<?= $tbl_paket['n2D1N']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n3D2N">3D2N</label>
                    <input style="margin-left:40px;" type="text" name="n3D2N" id="n3D2N" value="<?= $tbl_paket['n3D2N']; ?>">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n4D3N">4D3N</label>
                    <input style="margin-left:40px;" type="text" name="n4D3N" id="n4D3N" value="<?= $tbl_paket['n4D3N']; ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="keterangan">keterangan paket</label>
            <input type="text" name="keterangan" class="form-control" id="keterangan" rows="3" value="<?= $tbl_paket['keterangan']; ?>">
        </div>
        <div class="form-group">
            <img src="<?= base_url('upload/') . $tbl_paket['image']; ?>" class="img-thumbnail" width="200" height="200"> <br />
            <label for="image">Update Gambar</label>

            <input type="file" name="image" class="form-control-file" id="image" value="Ubah Data"> <br />
            <input type="submit" class="btn btn-primary value " value="Ubah Data" />
            <?= form_close() ?>
        </div>

        <!-- <div class="form-group">
                <label for="gambar">Masukan Gambar</label>
                <input type="file" class="form-control-file" id="gambar">
            </div> -->
        </form>

    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->