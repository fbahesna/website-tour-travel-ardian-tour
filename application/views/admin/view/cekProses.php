<div class="container">
    <div class="col">
        <h2 style="margin-left:380px;"><?= $title ?></h2>
    </div>
    <hr />
    <div class="col">
        <h5>Data Pemesan</h5>
    </div>
    <hr />
    <div class="col">
        <form action="<?= base_url('pesan/proses') ?>" method="post">
            <div class="form-group row">
                <label for="namalengkap" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" name="namalengkap" id="namalengkap" value="<?= $laporan['namalengkap']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" name="email" id="email" value="<?= $laporan['email']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="notelepon" name="notelepon" class="col-sm-2 col-form-label">Nomor Telepon</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="notelepon" name="notelepon" value="<?= $laporan['notelepon']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" name="alamat" id="alamat" value="<?= $laporan['alamat']; ?>">
                </div>
            </div>
            <hr />
            <div class="col">
                <h5>Data Tujuan Wisata</h5>
            </div>
            <hr />
            <div class="form-group row">
                <label for="tujuan" name="tujuan" class="col-sm-2 col-form-label">Tujuan</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="tujuan" name="tujuan" value="<?= $laporan['tujuan_wisata']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="durasi" name="durasi" class="col-sm-2 col-form-label">Durasi</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="durasi" name="durasi" value="<?= $laporan['durasi']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="keberangkatan" name="keberangkatan" class="col-sm-2 col-form-label">Keberangkatan</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="keberangkatan" name="keberangkatan" value="<?= $laporan['date']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="date_created" name="date_created" class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="date_created" name="date_created" value="<?= $laporan['date_created']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="peserta" name="peserta" class="col-sm-2 col-form-label">Peserta</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="peserta" name="peserta" value="<?= $laporan['qty']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="voucher" name="voucher" class="col-sm-2 col-form-label">Voucher</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="voucher" name="voucher" value="<?= $laporan['voucher']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="total_bayar" name="total_bayar" class="col-sm-2 col-form-label">Total bayar</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="total_bayar" name="total_bayar" value="<?= $laporan['total_bayar']; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-success btn-lg btn-block">Proses</button>
        </form>
    </div>
</div>