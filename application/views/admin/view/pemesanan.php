<div class="container-fluid mt-5">
  <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

  <div class="div row">
    <div class="col-lg">
      <?= $this->session->flashdata('message'); ?>

      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Tujuan</th>
            <th scope="col">Durasi</th>
            <th scope="col">Nama Lengkap</th>
            <th scope="col">Email</th>
            <th scope="col">No Telepon</th>
            <th scope="col">Alamat</th>
            <th scope="col">Waktu Pemesanan</th>
            <th scope="col">Keberangkatan</th>
            <th scope="col">Peserta</th>
            <?php if ($this->db->get_where('checkout', ['voucher' => $this->input->post('voucher')])) : ?>
              <th scope="col">Voucher</th>
            <?php endif; ?>
            <th scope="col">Sub Total</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($pemesanan as $pm) : ?>
            <tr>
              <td scope="row"><?= $i; ?></td>
              <td><?= $pm['tujuan_wisata'];   ?></td>
              <td>Rp.<?= number_format($pm['durasi'], 0, ',', '.'); ?></td>
              <td><?= $pm['namalengkap'];   ?></td>
              <td><?= $pm['email'];   ?></td>
              <td><?= $pm['notelepon'];   ?></td>
              <td><?= $pm['alamat'];   ?></td>
              <td><?= $pm['date_created'];   ?></td>
              <td><?= $pm['date'];   ?></td>
              <td><?= $pm['qty'];   ?></td>
              <?php if ($this->db->get_where('checkout', ['voucher' => $this->input->post('voucher')])) : ?>
                <td><?= $pm['voucher'];   ?></td>
              <?php endif; ?>
              <td>Rp.<?= number_format($pm['total_bayar'], 0, ',', '.'); ?>
              <td>
                <a href="<?= base_url(); ?>pesan/editCheckout/<?= $pm['id']; ?>" class="badge badge-warning">Edit</a>
                <a href="<?= base_url(); ?>pesan/hapusCheckout/<?= $pm['id']; ?>" class="badge badge-danger" onclick="return confirm('PERHATIAN!!! Anda yakin ingin menghapus data ini?');">Batal</a>
                <a href="<?= base_url(); ?>transaksi/index/<?= $pm['id']; ?>" class="badge badge-info">Cetak</a>
                <a onclick="return confirm('Proses Pesanan ini?');" href="<?= base_url('pesan/prosesPesanan') ?>/<?= $pm['id']; ?>" class="btn btn-sm btn-success">Proses</a>
              </td>
            </tr>
            <?php $i++; ?>
          <?php endforeach; ?>

        </tbody>
      </table>