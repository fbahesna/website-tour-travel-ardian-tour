<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="container-fluid col-lg-7">
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
        <?= form_open_multipart('admin/inputdata'); ?>

        <?= $this->session->set_flashdata('flash'); ?>

        <div class="form-group">
            <label for="paket_tour">
                <h5>paket Tour</h5>
            </label>
            <input type="text" name="paket_tour" class="form-control" id="paket_tour">
            <small class="form-text text-danger"><?= form_error('paket_tour'); ?></small>
        </div>
        <div class="form-group">
            <label for="tujuan">
                <h5>Tujuan</h5>
            </label>
            <input type="text" name="tujuan" class="form-control" id="tujuan">
            <small class="form-text text-danger"><?= form_error('tujuan'); ?></small>
        </div>
        <div class="border">
            <label>
                <h5>Harga</h5>
            </label>
            <div class="row">
                <div class="col">
                    <label for="one_day">One Day</label>
                    <input style="margin-left:20px;" type="text" name="one_day" id="one_day">
                    <small class="form-text text-danger"><?= form_error('one_day'); ?></small>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n2D1N">2D1N</label>
                    <input style="margin-left:40px;" type="text" name="n2D1N" id="n2D1N">
                    <small class="form-text text-danger"><?= form_error('n2D1N'); ?></small>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n3D2N">3D2N</label>
                    <input style="margin-left:40px;" type="text" name="n3D2N" id="n3D2N">
                    <small class="form-text text-danger"><?= form_error('n3D2N'); ?></small>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="n4D3N">4D3N</label>
                    <input style="margin-left:40px;" type="text" name="n4D3N" id="n4D3N">
                    <small class="form-text text-danger"><?= form_error('n4D3N'); ?></small>
                </div>
            </div>
        </div>
        <!-- <div class="form-group">
            <label for="harga">Harga</label>
            <input type="text" name="harga" class="form-control" id="harga">
        </div> -->
        <div class="form-group">
            <label for="keterangan">
                <h5>Keterangan</h5>
            </label>
            <textarea type="text" name="keterangan" class="form-control" id="keterangan" rows="3"></textarea>
            <small class="form-text text-danger"><?= form_error('keterangan'); ?></small>
        </div>
        <div class="form-group">
            <label for="image">
                <h5>Masukkan Gambar</h5>
            </label>
            <input type="file" name="image" id="image" class="form-control-file">
            <small class="form-text text-danger"><?= form_error('image'); ?></small> <br />
            <input type="submit" class="btn btn-primary" value="Simpan" />
        </div>
        <?= form_close() ?>

        <!-- <div class="form-group">
                <label for="gambar">Masukan Gambar</label>
                <input type="file" class="form-control-file" id="gambar">
            </div> -->
        </form>
    </div>
</div>
<!-- /.container-fluid -->

</div>


<!-- End of Main Content -->