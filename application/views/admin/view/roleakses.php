<div class="container-fluid">
  <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

  <div class="div row">
    <div class="col-lg-6">


      <?= $this->session->flashdata('message'); ?>

      <h4>Role: <?= $role['role']; ?></h4>
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Menu</th>
            <th scope="col">Akses</th>

          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($menu as $m) : ?>
            <tr>
              <td scope="row"><?= $i; ?></td>
              <td><?= $m['menu'];   ?></td>
              <td>
                <div class="form-check">
                  <!-- Helper-->
                  <input class="form-check-input" type="checkbox" <?= cek_akses($role['id'],  $m['id']);  ?> data-role=" <?= $role['id']; ?>" data-menu=" <?= $m['id']; ?>">
                  <!---Data untuk Jquery---->

                </div>
              </td>
            </tr>
            <?php $i++; ?>
          <?php endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>



</div>