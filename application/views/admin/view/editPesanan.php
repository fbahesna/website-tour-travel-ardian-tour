<!-- Begin Page Content -->
<!DOCTYPE html>
<html>
<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- Page Heading -->
    <div class="row">
        <div class="container fluid">

            <form action="<?= base_url(); ?>pesan/simpanEditCheckout/<?= $checkout['id']; ?>" method="post">

                <input type="hidden" name="id" value="<?= $checkout['id']; ?>">
                <div class="form-group">
                    <label for="tujuan_wisata">Tujuan Wisata</label>
                    <input type="text" name="tujuan_wisata" class="form-control" id="tujuan_wisata" value="<?= $checkout['tujuan_wisata']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="namalengkap">Nama Lengkap</label>
                    <input type="text" name="namalengkap" class="form-control" id="namalengkap" value="<?= $checkout['namalengkap']; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" value="<?= $checkout['email']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="durasi">durasi</label>
                    <input type="text" name="durasi" class="form-control" id="durasi" value="<?= $checkout['durasi']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="date">Keberangkatan</label>
                    <input type="text" name="date" class="form-control" id="date" value="<?= $checkout['date']; ?>">
                </div>
                <div class="form-group">
                    <label for="qty">Peserta</label>
                    <input type="text" name="qty" class="form-control" id="qty" value="<?= $checkout['qty']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="notelepon">Nomor Telepon</label>
                    <input type="text" name="notelepon" class="form-control" id="notelepon" value="<?= $checkout['notelepon']; ?>">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" rows="3" value="<?= $checkout['alamat']; ?>">
                </div>

                <input type="submit" class="btn btn-primary value " value="Ubah Data" />

        </div>
        </form>
    </div>

</div>
</div>
<!-- /.container-fluid -->

</div>

</html>
<!-- End of Main Content -->