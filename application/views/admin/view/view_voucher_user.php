<body style="margin-top:120px;">
    <div class="container bg-light">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 style="  font-family: 'Noto Serif', serif;"><?= $title; ?></h2>
                <p>Copy dan paste code voucher di form checkout</p>
            </div>
        </div>
        <div class="row">
            <?php foreach ($getVoucher as $V) : ?>
                <div class="col-md-5">
                    <div class="card" style="width: 18rem; margin-top:20px; background-color:azure;">
                        <div class="card-body">
                            <h1 class="card-subtitle mb-2 text-muted">Rp.<?= number_format($V['diskon'], 0, ',', '.'); ?></h1>
                            <h4 title="Copy This Code" class="card-title bg-light"><?= $V['id']; ?></h4>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>