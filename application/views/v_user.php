<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo base_url() . "assets/"; ?>img/logoardiantour.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title><?= $title; ?></title>
    <link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/social.css">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
			CSS
			============================================= -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/main.css">

</head>

<body>

    <!--Informasi User-->

    <a>
        <span class="mr-2 d-none d-lg-inline text-gray-900 small"><?= $user['nama']; ?></span>

    </a>
    <!-- Dropdown - User Information -->



    <a href="<?= base_url('auth/logout'); ?>" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Keluar</a>

    <!--menu user-->
    </div>
    </nav>
    </header><!-- #header -->


    <!-- start banner Area -->

    <!-- End banner Area -->

    <!-- Start popular-destination Area -->

    <!-- End popular-destination Area -->


    <!-- Start price Area -->




    <?php $this->load->view('templates/user/user_pagination'); ?>




    <!-- Start other-issue Area -->

    <!-- End other-issue Area -->


    <!-- Start testimonial Area -->

    <!-- End testimonial Area -->

    <!-- Start home-about Area -->

    <!-- End home-about Area -->


    <!-- Start social Media -->

    <?php $this->load->view('socialMedia'); ?>
    <?php $this->load->view('Youtube'); ?>

    <!-- End social Media -->



    <!-- start footer Area -->

    <!-- End footer Area -->

    <script src="<?php echo base_url() . "assets/"; ?>js/vendor/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/popper.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/vendor/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/easing.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/hoverIntent.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/superfish.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/mail-script.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>js/main.js"></script>
</body>

</html>