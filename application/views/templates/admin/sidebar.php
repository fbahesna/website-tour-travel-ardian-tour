<!-- Sidebar -->
<ul class="navbar-nav bg-dark sidebar sidebar-dark accordion" id="accordionSidebar">


    <!-- Sidebar - Brand -->
    <img src="<?php echo base_url() . "assets/"; ?>img/log1.png" width="220px" /></a>

    <!-- divider garis -->
    <!-- <hr class="sidebar-divider"> -->

    <!-- Divider -->
    <!------------------------------------------------------------------------------------ Query Menu-->
    <?php
    $role_id = $this->session->userdata('role_id');
    $Qmenu = "SELECT`menu_user`.`id`,`menu`
                FROM `menu_user` JOIN `user_access_menu`
                ON `menu_user`.`id` = `user_access_menu`.`menu_id`
            WHERE `user_access_menu`.`role_id` = $role_id
            ORDER BY `user_access_menu`.`menu_id` ASC
   ";
    $menu = $this->db->query($Qmenu)->result_array();

    ?>

    <!-------------------------------------------------------------------------------- looping menu-->
    <?php foreach ($menu as $m) : ?>
        <li class="nav-item">
            <hr class="sidebar-divider">
            <a class="nav-link">
                <span><?= $m['menu']; ?></span></a>
            <hr class="sidebar-divider ">
        </li>
        <?php
        $menuId = $m['id'];
        $Qsubmenu = "SELECT *
                FROM `user_sub_menu` JOIN `menu_user` 
                ON `user_sub_menu`.`menu_id` = `menu_user`.`id`
                WHERE `user_sub_menu`.`menu_id` = $menuId
                AND `user_sub_menu`.`is_active` = 1
";
        $submenu = $this->db->query($Qsubmenu)->result_array();
        ?>

        <!----------------------------------------------------------------------Looping SubMenu -->
        <?php foreach ($submenu as $sm) : ?>
            <?php if ($title == $sm['title']) : ?>
                <li class="nav-item active">
                <?php else : ?>
                <li class="nav-item">
                <?php endif; ?>
                <a class="nav-link pb-0" href="<?= base_url($sm['url']); ?>">
                    <i class="<?= $sm['icon']; ?>"></i>
                    <span><?= $sm['title']; ?></span></a>
            </li>
        <?php endforeach; ?>



    <?php endforeach; ?>
    <hr class="sidebar-divider mt-3">

    <!-- Sidebar Toggler (Sidebar) -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Logout</span></a>
    </li>
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar --      