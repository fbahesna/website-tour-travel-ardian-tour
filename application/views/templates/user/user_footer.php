</div>
<!-- start footer Area -->
<footer class="footer-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="single-footer-widget">
					<h6>About Ardian Tour</h6>
					<p>
						Ardian Tour merupakan perusahaan yang bergerak di Bidang Jasa Biro Perjalan Tour yang berpusat di Purworejo sejak awal tahun 2014.
					</p>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="single-footer-widget">
					<div class="row">
						<div class="col">
							<ul>
								<li><a href="<?= base_url('auth'); ?>">Beranda</a></li>
								<li><a href="#">Paket Tour</a></li>
								<li><a href="<?= base_url('promo'); ?>">Promo</a></li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="row footer-bottom d-flex justify-content-between align-items-center">
			<p class="col-lg-8 col-sm-12 footer-text m-0">
				Copyright &copy; Ardian Tour <?= date('Y'); ?> </p>
			<div class="col-lg-4 col-sm-12 footer-social">
				<a href="www.facebook.com/ardiantourtravel/"><i class="fa fa-fw fa-facebook"></i></a>
				<a href="https://www.instagram.com/ardiantourofficial/"><i class="fa fa-fw fa-instagram"></i></a>
			</div>
		</div>
	</div>
</footer>
<!-- End footer Area -->

<script src="<?php echo base_url() . "assets/"; ?>js/vendor/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/popper.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/vendor/bootstrap.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/easing.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/hoverIntent.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/superfish.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/mail-script.js"></script>
<script src="<?php echo base_url() . "assets/"; ?>js/main.js"></script>
<!-- Pelanggan dapat memilih tanggal sebelum hari ini -->
<!-- <script type="text/javascript">
	$(function() {
		$('.start').datepicker();
	});
</script> -->
<script>
	var date = new Date();
	date.setDate(date.getDate());

	$("#start").datepicker({
		minDate: 0,
		dateFormat: "dd-mm-yy",
		defaultDate: date,
		onSelect: function() {
			selectedDate = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
		}
	});

	$("#start").datepicker("setDate", date);
</script>

</body>

</html>