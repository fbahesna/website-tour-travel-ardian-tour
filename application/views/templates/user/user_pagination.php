<div id="pakettour">
    <section class="price-area section-gap" style="margin-top: -120px;">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center"><br />
                        <br /><br />
                        <?= $this->session->flashdata('message'); ?>
                        <h1 style="font-family:'Roboto Slab',serif;" class="mb-20 text-light">Paket Tour Kami</h1>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- ---->

                <?php foreach ($tbl_paket as $pkt) : ?>
                    <div class="card-deck">
                        <div class="card">
                            <td><img src="<?= base_url() ?>upload/<?= $pkt->image; ?>" width="280" height="220 "></td>

                            <div class="card-body">
                                <h5 class="card-title"><?= $pkt->paket_tour; ?></h5>
                                <p style="color:black" class="card-text">Start From Rp.<?= number_format($pkt->one_day); ?>/Person</p>


                                <?php if (!$this->session->userdata('email')) : ?>
                                    <a href="<?= base_url(); ?>pesan/konfirmasiPesan/<?= $pkt->id; ?>" class="btn btn-info float-right" data-target="#exampleModal" data-toggle="modal">Selengkapnya</a>

                            </div>
                            <!-- Modal -->
                            <div class="modal fade bg-dark" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content bg-warning">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                <h2>Perhatian!!!</h2>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-light bg-warning">
                                            <h4>
                                                Belum punya akun? daftar sekarang</h4>
                                        </div>
                                        <div class="modal-body text-light bg-success">
                                            <h4>
                                                Daftar dan dapatkan voucher potongan promo menarik</h4>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="<?= base_url('auth/login'); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Masuk</a>
                                            <a href="<?= base_url('auth/registration'); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Mendaftar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php elseif ($this->session->userdata('email')) : ?>
                            <a href="<?= base_url(); ?>pesan/konfirmasiPesan/<?= $pkt->id; ?>" style="font-family:'Roboto Slab',serif;" class="btn btn-info float-right">Selengkapnya</a>

                        </div>
                        <!-- Modal -->


                    <?php endif; ?>


                    </div>
                <?php endforeach; ?>
                <!-- -->
            </div>
        </div>
    </section>
</div>

<div style="margin-left: 600px; margin-top:-70px;">
    <a href="<?= base_url('paketAll'); ?>" class="btn btn-success">
        <h1 style="font-family:'Roboto Slab',serif;">Paket Lainnya</h1>
    </a>
</div>