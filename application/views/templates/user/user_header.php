<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= base_url() . "assets/"; ?>img/logoardiantour.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!--cssku-->
	<link rel="stylesheet" text="text/css" href="<?= base_url() . "assets/"; ?>css/badge.css">
	<!-- font google -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Serif&display=swap" rel="stylesheet">
	<!DOCTYPE html>
	<!-- Custom fonts for this template-->
	<link href="<?= base_url('vendor/sbadmin2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- V_pesan FOnt -->
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('vendor/sbadmin2/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?= base_url('vendor/sbadmin2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('vendor/sbadmin2/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
</head>


<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Ardian Tour</title>
<!-- font google -->
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
<!-- Font v_pesan -->
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<!--CSS ============================================= -->
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/linearicons.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>css/main.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<!-- HEADER -->

<body>
	<header id="header">
		<nav class="navbar navbar-expand-lg navbar-light bg-dark fixed-top">
			<a href="<?= base_url('auth'); ?>"><img src="<?php echo base_url() . "assets/"; ?>img/log1.png" width="230px" alt="" title="" /></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<p class="ml-20"><a href="<?= base_url('auth'); ?>" class="text-light" style="font-family: 'Roboto Slab', serif; margin-left:25px;">
					<h4>Beranda</h4>
				</a></p>

			<p class="ml-20"><a href="<?= base_url('paketAll') ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:-5px;">
					<h4>Paket Tour</h4>
				</a></p>
			<p class="ml-20"><a href="<?= base_url('Promo'); ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:5px;">
					<h4>Promo</h4>
				</a></p>
			<p class="ml-20"><a href="<?= base_url('about'); ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:5px;">
					<h4>About</h4>
				</a></p>


			<!-- Cek User Is Logged In -->

			<?php if ($this->session->userdata('email')) : ?>
				<p class="ml-20"><a href="<?= base_url() ?>pesan/tampilBySesi/<?= $user['id']; ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:-0px;">
						<h4>Cek Pesanan</h4>
					</a></p>
				<p class="ml-20"><a href="<?= base_url('user/ubahPassword'); ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:5px;">
						<h4>Ubah Password</h4>
					</a></p>
			<?php endif; ?>
			<!--END Cek User Is Logged In -->

			<p class="ml-20"><a href="<?= base_url('voucher/ambilVoucher'); ?>" class="text-light" style="font-family: 'Roboto Slab', serif;margin-left:5px;">
					<h4>Voucher</h4>
				</a></p>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					<div class="btn-group">
						<div class="btn-group">
				</ul>
				<!-- For Visitor -->
				<?php if (!$this->session->userdata('email')) : ?>
					<span class="navbar-text">
						<a href="<?= base_url('auth/login'); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Masuk</a>
						<a href="<?= base_url('auth/registration'); ?>" class="btn btn-success btn-sm" role="button" aria-pressed="true">Mendaftar</a>
					</span>
				<?php elseif ($this->session->userdata('email')) : ?>
					<span class="mr-2 d-none d-lg-inline text-gray-900 small">
						<h4 class="text-light">User: <?= $user['nama']; ?></h4>
					</span>
					<a href="<?= base_url('auth/logout'); ?>" class="btn btn-danger btn-sm" role="button" aria-pressed="true">Keluar</a>
				<?php endif; ?>
				<!-- Cek For Administrator Dashboard Button -->
				<?php if ($this->session->userdata('role_id') == 1) : ?>
					<a href="<?= base_url('admin'); ?>" class="btn btn-info btn-sm" role="button" aria-pressed="true">Dashboard</a>
				<?php endif; ?>
			</div>
		</nav>
	</header>
	<!-- #header -->
</body>