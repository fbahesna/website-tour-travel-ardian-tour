<div class="container mb-auto" style="margin-top:90px;">
    <a class="btn btn-danger" onclick="return confirm('Pesanan akan dibatalkan! Anda yakin?');" href="<?= base_url('user') ?> ">Batalkan Pemesanan</a>
</div>
<div class="container col-md-5 align-left border" style="background-color:whitesmoke;">
    <div class="container">
        <div class="col">
            <h3><?= $title; ?>:<br /><br />
        </div>
    </div>
    <h2><?= $tbl_paket['paket_tour']; ?></h2>
    </h3>
    <h1 class="h3 mb-4 text-gray-800"></h1>
    <!-- <?= $this->session->set_flashdata('message'); ?> -->
    <?= validation_errors(); ?>
    <form action="" method="post">
        <div class="form-group">
            <label for="namalengkap">
                <h4>Nama Lengkap</h4>
            </label>
            <input autocomplete="off" value="<?= $user['nama'] ?>" type="text" name="namalengkap" class="form-control" id="namalengkap" required>
        </div>
        <div class="form-group">
            <label for="email">
                <h4>Email</h4>
            </label>
            <input autocomplete="off" type="text" name="email" class="form-control" id="email" value="<?= $user['email']; ?>" readonly required>
        </div>
        <div class="form-group">
            <label for="notelepon">
                <h4>No Telepon (Yang Bisa Dihubungi)</h4>
            </label>
            <input autocomplete="off" type="text" name="notelepon" class="form-control" id="notelepon" required>
        </div>
        <div class="form-group">
            <label for="alamat">
                <h4>Alamat</h4>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" class="form-control" id="alamat" required rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="tujuan_wisata">
                <h4>Tujuan Wisata</h4>
            </label>
            <input type="text" name="tujuan_wisata" class="form-control" id="tujuan_wisata" value="<?= $tbl_paket['paket_tour']; ?>" required readonly>
        </div>
        <div class="form-group" style="margin-bottom:80px;">
            <label for="durasi">
                <h4>Durasi / Orang</h4>
            </label>
            <select class="form-control" name="durasi" id="durasi" required>
                <option>-</option>

                <option value="<?= $durasi['one_day']; ?>">1 Hari</option>
                <option value="<?= $durasi['n2D1N']; ?>">2Hari 1Malam</option>
                <option value="<?= $durasi['n3D2N']; ?>">3Hari 2Malam</option>
                <option value="<?= $durasi['n4D3N']; ?>">4Hari 3Malam</option>
            </select><br />
            <div class="row col-lg-5">
                <label for="qty">
                    <h4>Peserta</h4>
                </label>
                <input autocomplete="off" class="form-control" type="text" id="qty" name="qty" placeholder="Banyaknya peserta tour" required>
                <small class="form-text text-muted">Banyaknya peserta termasuk anda</small>
            </div>
            <div class="row col-lg-5">
                <label for="start">
                    <h4>Keberangkatan</h4>
                </label>
                <input autocomplete="off" class="form-control" type="text" id="start" name="start" required>
                <small class="form-text text-muted">Pilih tanggal Keberangkatan </small>
            </div>
            <div class="row col-lg-5">
                <label for="voucher">
                    <h4>Voucher</h4>
                </label>
                <input autocomplete="off" class="form-control" type="text" id="voucher" name="voucher" placeholder="Masukkan Kode Voucher...">
            </div>
            <input type="submit" class="btn btn-success float-right mt-4" value="Pesan" onclick="return confirm('Dengan ini saya menyetujui syarat&ketentuan  dari Ardian Tour');" />
    </form>
</div>
</div>
<div class="container">
    <div class="col">
        <table style="margin-right:50px;" class="table table-hover border-dark">
            <thead>
                <tr>
                    <th>
                        <h4>1 Hari</h4>
                    </th>
                    <th>
                        <h4>2Hari 1Malam</h4>
                    </th>
                    <th>
                        <h4>3Hari 2Malam</h4>
                    </th>
                    <th>
                        <h4>4Hari 3Malam</h4>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <h6>Rp<?= number_format($tbl_paket['one_day'], 0, ',', '.'); ?></h6>
                    </td>
                    <td>
                        <h6>Rp<?= number_format($tbl_paket['n2D1N'], 0, ',', '.'); ?></h6>
                    </td>
                    <td>
                        <h6>Rp<?= number_format($tbl_paket['n3D2N'], 0, ',', '.'); ?></h6>
                    </td>
                    <td>
                        <h6>Rp<?= number_format($tbl_paket['n4D3N'], 0, ',', '.'); ?></h6>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


<br /><br /><br />
<!-- <script type="text/javascript">
    getData()

    function getData() {
        $.ajax({
            type: 'POST',
            url: '<?= base_url('voucher/getData'); ?>',
            dataType: 'json',
            success: function(data) {
                var baris = '';
                for (var i = 0; i < data.length; i++) {
                    baris += '<tr>' +
                        '<td>' + data[i].id + '</td>' +
                        '<td>' + data[i].diskon + '</td>' +
                        '</tr>';
                }
                $('#target').html(baris);
            }
        })
    }
</script> -->