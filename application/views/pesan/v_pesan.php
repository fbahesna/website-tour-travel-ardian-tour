<!-- NAVBAR UNTUK USER -->
<link rel="stylesheet" href="<?= base_url() . "assets/" ?>css/v_pesan.css">
<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<div class="container">
  <h1 class="align-self-center text-judul">Konfirmasi Pemesanan Wisata <?= $tbl_paket['paket_tour']; ?> : </h1>
  <div class="card">
    <img src="<?= base_url() ?>./upload/<?= $tbl_paket['image']; ?>" class="img-fluid align-self-center" alt="Responsive image"></div>

  <body>
    <div class="container  border-dark">
      <div class="col-lg">
        <div class="row">
          <h3 class="text-judul mt-4"><?= $tbl_paket['paket_tour']; ?>:</h3>
          <hr>
        </div>
        <div class="row">
          <h4 class="text"><?= $tbl_paket['keterangan']; ?></h4>
        </div>
        <br />

        <div class="row">
          <h3 class="text-judul">Destinasi:</h3>
        </div>
        <hr>
        <div class="row">
          <h4 class="text"><?= $tbl_paket['tujuan']; ?></h4>
        </div>
        <hr>
        <div class="row">
          <h3 class="text-judul">Transportasi:</h3>
          <hr>
        </div>
        <!-- Transportasi -->

        <div class="row">
          <div class="col-lg-3">
            <div class="card" style="width: 13rem; height:17rem;">
              <img height="130px;" width="15px;" src="<?= base_url() . "kendaraan/"; ?>avanza.jpg" class="card-img-top">
              <div class="card-body">
                <i class="fas fa-sign fa-2x">
                  Avanza
                </i>
                <h6 class="card-text">Untuk 1-6 Orang</h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="card" style="width: 13rem; height:17rem;">
              <img height="130px;" width="15px;" src="<?= base_url() . "kendaraan/"; ?>elf.png" class="card-img-top">
              <div class="card-body">
                <i class="fas fa-sign fa-2x">
                  Elf/Hiace
                </i>
                <h6 class="card-text">Untuk 10-15 Orang</h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="card" style="width: 13rem; height:17rem;">
              <img height="130px;" width="40px;" src="<?= base_url() . "kendaraan/"; ?>medium.jpg" class="card-img-top">
              <div class="card-body">
                <i class="fas fa-sign fa-2x">
                  Medium Bus
                </i>
                <h6 class="card-text">Untuk 20-25/30-35 Orang</h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="card" style="width: 13rem; height:17rem;">
              <img height="130px;" width="15px;" src="<?= base_url() . "kendaraan/"; ?>bigbus.jpg" class="card-img-top">
              <div class="card-body">
                <i class="fas fa-sign fa-2x">
                  Big Bus
                </i>
                <h6 class="card-text">Untuk 45-50 Orang</h6>
              </div>
            </div>
          </div>
        </div><br />
        <!-- End -->
      </div>
    </div>
    <div class="col-lg-4">
      <table class="table table-hover border-dark">
        <thead>
          <tr>
            <th>
              <h4>One day</h4>
            </th>
            <th>
              <h4>2D1N</h4>
            </th>
            <th>
              <h4>3D2N</h4>
            </th>
            <th>
              <h4>4D3N</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h6>Rp<?= number_format($tbl_paket['one_day'], 0, ',', '.'); ?></h6>
            </td>
            <td>
              <h6>Rp<?= number_format($tbl_paket['n2D1N'], 0, ',', '.'); ?></h6>
            </td>
            <td>
              <h6>Rp<?= number_format($tbl_paket['n3D2N'], 0, ',', '.'); ?></h6>
            </td>
            <td>
              <h6>Rp<?= number_format($tbl_paket['n4D3N'], 0, ',', '.'); ?></h6>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </tbody>
    </table>
    <div class="float-right">
      <div class="text">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable">
          Syarat & Ketentuan
        </button>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle">Syarat & Ketentuan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h3> Pendaftaran & Pembayaran</h3><br />
            <h6>1. Uang muka pendaftaran yang dibayarkan kepada PT. Ardian Tour tidak dapat dikembalikan (down payment non-refundable).</h6>
            <br />
            <h6>2. Peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan demikian juga jika terdapat biaya lain seperti pembatalan hotel, kereta dan atau tiket pesawat yang terjadi karena adanya tenggat waktu yang belum tentu sesuai dengan waktu penyelesaian , dan juga biaya tour lainnya maka akan dibebankan kepada Peserta tour.</h6>
            <br />
            <h6>3.Pendaftaran tanpa disertai deposit bersifat tidak mengikat dan dapat dibatalkan tanpa pemberitahuan terlebih dahulu kepada Peserta.</h6>
            <br />
            <h6>
              4.Pelunasan biaya tour dilakukan 7 hari sebelum tanggal keberangkatan.</h6>
            <br />
            <h6>5.Bagi pendaftar yang berusia di atas 70 tahun atau memiliki keterbatasan fungsi anggota tubuh atau indera atau keterbatasan secara mental, wajib didampingi oleh anggota keluarga, teman atau saudara yang akan bertanggung jawab selama perjalanan tour.</h6>
            <br />
          </div>

        </div>
      </div>
    </div>
    <?php if (!$this->session->userdata('email')) : ?>
      <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#exampleModal">
        Check Out
      </button>

    <?php elseif ($this->session->userdata('email')) : ?>
      <a href="<?= base_url('pesan/CheckOut'); ?>/<?= $tbl_paket['id']; ?>" class="btn btn-success float-right">Check Out</a>
    <?php endif; ?>


    <!-- Modal Belum punya akun -->
    <div class="modal fade bg-dark" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content bg-warning">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              <h2>Perhatian!!!</h2>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-light bg-warning">
            <h4>
              Belum punya akun? daftar sekarang</h4>
          </div>
          <div class="modal-body text-light bg-success">
            <h4>
              Daftar dan dapatkan potongan harga paket hingga Rp.100.000</h4>
          </div>
          <div class="modal-footer">
            <a href="<?= base_url('auth/login'); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Masuk</a>
            <a href="<?= base_url('auth/registration'); ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Mendaftar</a>
          </div>
        </div>
      </div>
    </div>
    <div class="container"></div>