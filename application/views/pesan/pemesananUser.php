<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<div class="container-fluid mt-5">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="div row">
        <div class="col-lg">
            <?= $this->session->flashdata('message'); ?>
            <!-- Cek user pesan/belum -->
            <?php if (!$sesi) : ?>
                <div class="col-md-5" style="margin-left:400px;">
                    <h3 class="text-danger">Anda belum membuat pesanan!</h3>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col">
                    <a href="<?= base_url(); ?>transaksi/index/<?= $sesi['id']; ?>" class="badge badge-warning float-right">Cetak</a>

                </div>
            </div>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tujuan</th>
                        <th scope="col">Durasi</th>
                        <th scope="col">voucher</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Email</th>
                        <th scope="col">No Telepon</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Waktu Pemesanan</th>
                        <th scope="col">keberangkatan</th>
                        <th scope="col">Peserta</th>
                        <th scope="col">Sub Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <tr>
                        <td scope="row"><?= $i; ?></td>
                        <td><?= $sesi['tujuan_wisata'];   ?></td>
                        <td><?= number_format($sesi['durasi'], 0, ',', '.'); ?></td>
                        <td><?= $sesi['voucher'];   ?></td>
                        <td><?= $sesi['namalengkap'];   ?></td>
                        <td><?= $sesi['email'];   ?></td>
                        <td><?= $sesi['notelepon'];   ?></td>
                        <td><?= $sesi['alamat'];   ?></td>
                        <td><?= $sesi['date_created'];   ?></td>
                        <td><?= $sesi['date'];   ?></td>
                        <td><?= $sesi['qty'];   ?></td>
                        <td>Rp.<?= number_format($sesi['total_bayar'], 0, ',', '.')
                                ?></td>
                        <td>
                        </td>
                    </tr>
                    <?php $i++; ?>

                </tbody>
                <a href="<?= base_url() ?>pesan/deleteConfirm/<?= $user['id']; ?>" class="fas fa-fw fa-window-close fa-2x" onclick="return confirm('Anda yakin ingin membatalkan pesanan?')"></a>
            </table>