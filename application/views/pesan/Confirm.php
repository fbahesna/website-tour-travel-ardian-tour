<div class="container bg-light" style="margin-top:150px;">
    <div class="col">
        <div class="row">

            <h1 style="margin-left:420px;"><?= $title; ?></h1>
        </div>
        <h4><?= $this->session->flashdata('message'); ?></h4>
        <h2><?= $this->session->flashdata('flash'); ?></h2>
    </div>
    <div class="col-lg">
        <h5>-Pemesanan anda sudah berhasil dan tersimpan di database kami.</h5>
        <h5>-Mohon cek Email anda untuk melihat data yang telah kami kirim.</h5>
        <h5>-Terima kasih telah mempercayai kami sebagai penyedia layanan tour wisata.</h5>
        <table style="margin-top:10px; border-radius:10px;" class="table table-bordered table-dark">
            <thead>
                <tr>
                    <th scope="col">Nama Pemesan</th>
                    <th scope="col">Email</th>
                    <th scope="col">No Telepon</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Tujuan Wisata</th>
                    <th scope="col">Keberangkatan</th>
                    <th scope="col">Durasi</th>
                    <?php if ($this->input->post('voucher')) : ?>
                        <th scope="col">Voucher</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row"><?= $checkout->namalengkap ?></th>
                    <td><?= $checkout->email ?></td>
                    <td><?= $checkout->notelepon ?></td>
                    <td><?= $checkout->alamat ?></td>
                    <td><?= $checkout->tujuan_wisata ?></td>
                    <td><?= $checkout->date ?></td>
                    <td>Rp.<?= number_format((int) $checkout->durasi, 0, ',', '.'); ?> X <?= $this->input->post('qty'); ?> Orang</td>
                    <?php if ($this->input->post('voucher')) : ?>
                        <td>Rp.<?= number_format((int) $potongan->diskon, 0, ',', '.') ?></td>
                    <?php endif; ?>
                </tr>
            </tbody>
        </table>

        <h2>Total yang harus dibayar : Rp.<?= number_format($checkout->total_bayar, 0, ',', '.');
                                            // $a = array($checkout->durasi * $this->input->post('qty'));
                                            // echo " Rp = " . number_format(array_sum($a), 0, ',', '.') . "\n";
                                            ?>
            <br />
            <br />
            <div class="row">
                <div class="col-lg-4"><a class="btn btn-info" href="<?= base_url() ?>pesan/tampilBySesi/<?= $user['id']; ?>">Cek Pesanan</a></div>
            </div>

    </div><br /><br /><br /><br /><br /><br />