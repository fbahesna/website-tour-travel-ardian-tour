<link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/about.css">
<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<div class="container col-lg">
    <div class="row">
        <img src="<?= base_url('./assets/img/top-banner.jpg'); ?>" class="img-fluid" alt="Responsive image">
    </div>
</div>
<?php if ($this->session->userdata('role_id') == 1) : ?>
<!-- Button trigger modal -->
<i class="fas fa-fw fa-edit fa-2x" data-toggle="modal" data-target="#exampleModal">
</i>
<?php endif; ?>
<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center mb-5">
                <h2 class="titleSection mt-3"><?= $about['judul']; ?></h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-6">
                <img class="img-thumbnail embed-responsive" src="<?= base_url(); ?>assets/img/log1.png">
            </div>
            <div class="col-lg-6 fluid">
                <h6 class="desc"><?= $about['about']; ?></h6>
            </div>
        </div>
    </div>
</section>
<!-- Cek untuk edit about -->
<section id="section" class="contact bg-light">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-12 text-center">
                <h2 class="titleSection mt-3">Kontak Kami</h2>
            </div>
        </div>
    </div>
    <div class="row  text-center">
        <div class="col-lg-6"> <i class="fab fa-fw fa-whatsapp fa-7x wa-icon-about"></i></div>
        <div class="col-lg-6 "> <i class="far fa-fw fa-envelope fa-7x mail-icon-about"></i></div>
    </div>
    <div class="row text-center">
        <div class="col-lg-6">
            <h4 style="margin-left:5px; margin-top:5px;"> </h4> <br />
            <a href="https://wa.me/6281227426947?text=Silahkan%20mengajukan%20pertanyaan%20mengenai%20paket%20wisata%20yang%20disediakan%20oleh%20Ardian%20Tour" class="btn btn-success wa" target="_blank">Kirim Pesan</a>
        </div>
        <div class="col-lg-6">
            <h4 style="margin-left:5px; margin-top:5px;"></h4> <br />
            <a href="<?= base_url('about/clientSendEmail') ?>" class="btn btn-danger" target="_blank">Kirim Email</a>
            <?= $this->session->flashdata('pesan'); ?>
        </div>
    </div>

    </h6>
</section>
<!-- Peta Lokasi -->
<div class="container">
    <div class="col-lg-12 text-center">
        <h2 class="titleSection mt-3 mb-4">Peta Lokasi Ardian Tour</h2>
    </div>
    <div class="col-lg-12 mb-5">
        <div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.772815611887!2d110.01190533780915!3d-7.707510649714696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6caae2eeb01ebea6!2sArdian+Tour!5e0!3m2!1sid!2sid!4v1561626157844!5m2!1sid!2sid" width="1100" height="450" frameborder="20px" style="border:20px" allowfullscreen></iframe>
        </div>
    </div>
</div>
<!-- End Peta Lokasi -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit About</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('about/editAbout'); ?>" method="post">
                    <div class="form-group">
                        <label for="judul">About</label>
                        <input type="text" class="form-control" name="judul" id="judul" aria-describedby="editAbout" value="<?= $about['judul']; ?>">

                        <small for="about" id="emailHelp" class="form-text text-muted">Edit About</small>
                        <input type="text" class="form-control" name="about" id="about" aria-describedby="editAbout" value="<?= $about['about']; ?>">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>