<?php
//============================================================+
// File name   : example_005.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 005 for TCPDF class
//               Multicell
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Multicell
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
// require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Ardian Tour');
$pdf->SetTitle('Ardian Tour');
$pdf->SetSubject('TCPDF ');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' ', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 10);

// add a page
$pdf->AddPage();

// set cell padding
$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
$pdf->setCellMargins(1, 1, 1, 1);

// set color for background
$pdf->SetFillColor(255, 255, 127);

// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

$title = <<<EDD
<h2>Bukti Data Pemesan</h2>
EDD;
$pdf->writeHTMLCell(0, 0, '', '', $title, 0, 1, 0, true, 'C', true);
$table = '<table style="border:1px solid #000; padding:6px;">';

$table .= '<tr style="background-color: #343A40;">
            <th  style="border:1px solid #000; padding:6px; color:white;">Tujuan</th>
            <th  style="border:1px solid #000; padding:6px; color:white;">Nama Pemesan</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Email</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Nomor Telepon</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Alamat</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Potongan</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Tanggal Pemesanan</th>
            <th style="border:1px solid #000; padding:6px; color:white;">Total Pembayaran</th>
         </tr>';


$table .= '<tr>
                <td style="border:1px solid #000; padding:6px;"> ' . $transaksiPromo['tujuan_promo'] . '</td>
                <td style="border:1px solid #000; padding:6px;">Sdr. ' . $transaksiPromo['nama'] . '</td>
                <td style="border:1px solid #000; padding:6px;">' . $transaksiPromo['email'] . '</td>
                <td style="border:1px solid #000; padding:6px;">' . $transaksiPromo['notelepon'] . '</td>
                <td style="border:1px solid #000; padding:6px;">' . $transaksiPromo['alamat'] . '</td>
                <td style="border:1px solid #000; padding:6px;">Rp.' . number_format($transaksiPromo['potongan_promo'], 0, ',', '.') . '</td>
                <td style="border:1px solid #000; padding:6px;">' . $transaksiPromo['date_created'] . '</td>
                <td style="border:1px solid #000; padding:6px;">Rp.' . number_format($transaksiPromo['total_bayar'], 0, ',', '.') . '</td>
                
                
               
    
            </tr>';

$table .= '</table>';
$pdf->writeHTMLCell(0, 0, '', '', $table, 0, 1, 0, true, 'C', true);

$syarat = <<<EDD
<h2>Syarat&Ketentuan</h2><br/>

<h4>1. Uang muka pendaftaran yang dibayarkan kepada PT. Ardian Tour tidak dapat dikembalikan (down payment non-refundable).</h4>
<br />
<h4>2. Peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan demikian juga jika terdapat biaya lain seperti pembatalan hotel, kereta dan atau tiket pesawat yang terjadi karena adanya tenggat waktu yang belum tentu sesuai dengan waktu penyelesaian , dan juga biaya tour lainnya maka akan dibebankan kepada Peserta tour.</h4>
<br />

<h4>3. Pendaftaran tanpa disertai deposit bersifat tidak mengikat dan dapat dibatalkan tanpa pemberitahuan terlebih dahulu kepada Peserta.</h4>
<br />
<h4>
4. Pelunasan biaya tour dilakukan 7 hari sebelum tanggal keberangkatan.</h4>
<br />


<br />
<h4>5. Bagi pendaftar yang berusia di atas 70 tahun atau memiliki keterbatasan fungsi anggota tubuh atau indera atau keterbatasan secara mental, wajib didampingi oleh anggota keluarga, teman atau saudara yang akan bertanggung jawab selama perjalanan tour.</h4>
<br />
EDD;
$pdf->writeHTML($syarat);









// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('Bukti Data Pemesan.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
