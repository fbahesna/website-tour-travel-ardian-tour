<!-- Link CSS paketAll -->
<link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/paketAll.css">
<!-- Container -->
<div class="container-bg">
    <div class="container">
        <div class="row">
            <?php foreach ($paket as $pkt) : ?>
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img src="<?= base_url() ?>upload/<?= $pkt->image; ?> " height="200px" width="286px">
                        <div class="card-body">
                            <h5 class="card-title"><?= $pkt->paket_tour ?></h5>
                            <p class="card-text">Start From Rp. <?= number_format($pkt->one_day, 0, ',', '.'); ?>/Person</p>
                            <a href="<?= base_url(); ?>pesan/konfirmasiPesan/<?= $pkt->id; ?>" class="btn btn-primary">Selengkapnya</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-lg" style="margin-top:20px;">
        <?= $pagination; ?>
    </div>
</div>


</div>
</div>
</div>