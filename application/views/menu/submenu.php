<div class="container-fluid">
  <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

  <div class="div row">
    <div class="col-lg">
      <?php if (validation_errors()) : ?>
        <div class="alert alert-danger" role="alert">
          <?= validation_errors(); ?>
        </div>
      <?php endif; ?>

      <?= $this->session->flashdata('message'); ?>
      <?= $this->session->flashdata('flash'); ?>

      <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal"> Tambah Submenu Baru </a>

      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Title</th>
            <th scope="col">Menu</th>
            <th scope="col">url</th>
            <th scope="col">Icon</th>
            <th scope="col">Active</th>
            <th scope="col">Action</th>

          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($subMenu as $sm) : ?>
            <tr>
              <td scope="row"><?= $i; ?></td>
              <td><?= $sm['title'];   ?></td>
              <td><?= $sm['menu'];   ?></td>
              <td><?= $sm['url'];   ?></td>
              <td><?= $sm['icon'];   ?></td>
              <td><?= $sm['is_active'];   ?></td>

              <td>

                <a href="<?= base_url(); ?>menu/editSubMenu/<?= $sm['id']; ?>" class="badge badge-success" data-toggle="modal" data-target="#editSubMenuModal">Edit</a>

                <a href="<?= base_url(); ?>menu/hapusSubMenu/<?= $sm['id']; ?>" class="badge badge-danger" onclick="return confirm('PERHATIAN!!! Anda yakin ingin menghapus data ini?');">Hapus </a>

              </td>
            </tr>
            <?php $i++; ?>
          <?php endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>


</div>

<!---MODAL--->

<div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newSubMenuModalLabel">Tambahkan Submenu Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>



      <!--submenu-->


      <form action="<?= base_url('menu/submenu'); ?>" method="post">
        <div class="modal-body">

          <div class="form-group">
            <input type="text" class="form-control" id="title" name="title" placeholder="Nama Submenu">
            <small class="form-text text-danger"><?= form_error('title'); ?></small>
          </div>

          <div class="form-group">
            <select name="menu_id" id="menu_id" class="form-control">
              <option>Pilih Menu Akses</option>
              <?php foreach ($menu as $m) : ?>
                <option value="<?= $m['id']; ?>"><?= $m['menu']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>


          <div class="form-group">
            <input type="text" class="form-control" id="url" name="url" placeholder="url Submenu">
            <small class="form-text text-danger"><?= form_error('url'); ?></small>
          </div>

          <div class="form-group">
            <input type="text" class="form-control" id="icon" name="icon" placeholder="icon Submenu">
            <small class="form-text text-danger"><?= form_error('icon'); ?></small>
          </div>

          <div class="form-group">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" value="1" name="is_active" id="is_active" checked>
              <label for="is_active" class="form-check-label">
                Active?
              </label>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambahkan</button>
              </div>


            </div>

          </div>
        </div>