<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<head>
    <title>Preview PDF</title>
    <style>
        table {
            border-collapse: collapse;
            table-layout: fixed;
            width: 630px;
        }

        table td {
            word-wrap: break-word;
            width: 20%;
        }
    </style>
</head>

<body>
    <div class="container-fluid mt-5">
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="div row">
            <div class="col-lg">
                <?= $this->session->flashdata('message'); ?>

                <table class="table table-hover">
                    <thead>
                        <tr>

                            <th scope="col">Tujuan</th>
                            <th scope="col">Nama Pemesan</th>
                            <th scope="col">Email</th>
                            <th scope="col">No Telepon</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Waktu Pemesanan</th>
                            <th scope="col">Action</th>


                        </tr>
                    </thead>
                    <tbody>


                        <tr>

                            <td><?= $transaksi['tujuan_wisata'];   ?></td>
                            <td><?= $transaksi['namalengkap'];   ?></td>
                            <td><?= $transaksi['email'];   ?></td>
                            <td><?= $transaksi['notelepon'];   ?></td>
                            <td><?= $transaksi['alamat'];   ?></td>
                            <td><?= $transaksi['date_created'];   ?></td>


                            <td>

                                <a href="<?= base_url(); ?>transaksi/cetak/<?= $transaksi['id']; ?>" class="badge badge-info">Cetak</a>

                        </tr>



                    </tbody>
                </table>
                </tbody>
                </table>
</body>

</html>