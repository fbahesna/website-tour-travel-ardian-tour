	<!-- #header -->
	<!-- CSS PANEL -->
	<link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/panel.css">
	<link rel="stylesheet" href="<?= base_url() . "assets/"; ?>css/social.css">
	<link href="<?= base_url('assetsadmin/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- start banner Area -->
	<section class="banner-area relative" id="home">

		<div class="overlay overlay-bg"></div>

		<div class="container">
			<div class="row fullscreen align-items-center justify-content-between" style="margin-top:-90px;">
				<div class="col-lg-6 col-md-6 banner-left">
					<?= $this->session->flashdata('message'); ?>
					<h6 class="text-white">Wise Choice For Your Traveling</h6>

					<a href="<?= base_url('auth'); ?>"><img src="<?php echo base_url() . "assets/"; ?>img/log1.png" width="430px" alt="" title="" /></a>
					<!-- <p>Jl.Let Jend Suprapto no 27,Tuksongo,Purworejo 54111</p> -->
				</div>
				<div class="col-lg-6 col-md-10 banner-right">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<!--Carousel Gambar -->
						<div id="carouselExampleIndicators" class="carousel slide rounded-pill" data-ride="carousel">
							<ol class=" carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner " style=" border-radius:25px; box-shadow:2 3px 15px rgba(0,0,0,0.5);">
								<div class="carousel-item active">

									<img src="upload/jatimpark3.jpg" class="d-block w-100 " style="height:300px; width:200px; " alt="...">
									<div class="carousel-caption d-none d-md-block">
										<h4 class="text-light">Dieng & Bukit Sikunir</h4>
									</div>
								</div>
								<div class="carousel-item">
									<img src="upload/Malioboro.jpg" style="height:300px; width:200px;  " class="d-block w-100" alt="...">
									<div class="carousel-caption d-none d-md-block">
										<h4 class="text-light">Malioboro</h4>
									</div>

								</div>
								<div class="carousel-item">
									<img src="upload/Bromo.jpg" style="height:300px; width:200px; " class="d-block w-100" alt="...">
									<div class="carousel-caption d-none d-md-block">
										<h4 class="text-light">Bromo</h4>
									</div>

								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</ul>

	</section>

	<!--Container-->
	<div class="container">
		<!-- Info panel -->
		<div class="row justify-content-center">
			<div class="col-lg-10 info-panel">
				<div class="row" style="	box-shadow: 0 3px 20px rgba(0, 0, 0, 0.5); 
    						border-radius: 20px;
							margin-top: -105px;
							
							background-color: #243847fd;
							padding: 20px;
">
					<div class="col lg">
						<img src="upload/index/uang.png" height="90" width="90" class="info-img1">
						<h4 class="text-1">Harga Bersahabat</h4>
						<h6 class="text-1">Kami memiliki harga yang sesuai dengan kebutuhan anda</h6>

					</div>
					<div class="col lg">
						<img src="upload/index/pesan.png" height="90" width="90" class="info-img1">
						<h4 class="text-1">Pemesanan Mudah</h4>
						<h6 class="text-1">Memesan paket wisata jadi lebih mudah</h6>

					</div>
					<div class="col lg">
						<img src="upload/index/petugas.png" height="90" width="90" class="info-img1">
						<h4 class="text-1">Petugas Ramah</h4>
						<h6 class="text-1">Petugas kami siap membantu dalam perjalanan anda</h6>

					</div>
				</div>


			</div>

		</div>
		<!-- Akhir Info Panel -->




	</div>
	<!-- end container -->










	<!-- Pagination Paket -->
	<?php $this->load->view('templates/user/user_pagination'); ?>
	<!--END Pagination Paket -->

	<!-- Facebook API -->
	<?php
	$Token = 'EAAGWcIme8R0BAFkVZCB9aXiIARdMKXQvxQ9D0ZC78qsGUBqod7Ln9sGiVNn4MWhmK16TDsZCPrHCMo9FJ9VMYLeKX1ohUDDzENkTaxSS8IL9vLV6uXcnfL2Mfw6EjSxZBrqTNuy4xGjviQDCPRo1NLhqzGBlxLjTHB8MMhP6Gq6H6GrymTRjB1ZCU0jZCKa6EZD';
	?>


	<!-- Instagram API -->
	<?php $this->load->view('socialMedia') ?>
	<!-- END Instagram API -->

	<!-- YOUTUBE Section -->
	<?php $this->load->view('Youtube'); ?>
	<!-- End recent-blog Area -->

	<!-- start footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">

			<div class="row">
				<div class="col-lg-3">
					<div class="single-footer-widget">
						<h6>Tentang Ardian Tour</h6>
						<p>
							Ardian Tour merupakan perusahaan yang bergerak di Bidang Jasa Biro Perjalan Tour yang berpusat di Purworejo sejak awal tahun 2014.
						</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="single-footer-widget">
						<div class="row">
							<div class="col">
								<ul>
									<li><a href="#home">Beranda</a></li>
									<li><a href="<?= base_url('paketAll'); ?>">Paket Tour</a></li>
									<li><a href="<?= base_url('about'); ?>">About</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row footer-bottom d-flex justify-content-between align-items-center">
				<p class="col-lg-8 col-sm-12 footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy; Ardian Tour <?= date('Y'); ?> All rights reserved </p>
				<div class="col-lg-4 col-sm-12 footer-social">
					<a href="www.facebook.com/ardiantourtravel/"><i class="fa fa-facebook"></i></a>
					<a href="https://www.instagram.com/ardiantourofficial/"><i class="fa fa-instagram"></i></a>
				</div>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->

	<script src="<?php echo base_url() . "assets/"; ?>js/vendor/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/popper.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/vendor/bootstrap.min.js"></script>

	<script src="<?php echo base_url() . "assets/"; ?>js/jquery-ui.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/easing.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/hoverIntent.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/superfish.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/jquery.ajaxchimp.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/jquery.nice-select.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/mail-script.js"></script>
	<script src="<?php echo base_url() . "assets/"; ?>js/main.js"></script>
	</body>

	</html>