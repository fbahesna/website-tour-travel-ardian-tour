<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller

{
    public function __construct()
    {
        parent::__construct();
        // cek_login();
        $this->load->Model('M_crud');
        if (!$this->session->userdata('role_id')) {
            redirect('auth/blocked');
        } elseif ($this->session->userdata('role_id') == 2) {
            redirect('auth/blocked');
        }
    }
    public function index()                                //-------------------------------------------------------------///
    {
        $data['title'] = 'Tabel Paket';
        $data['tbl_paket'] = $this->M_crud->tampil_data()->result();

        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->session->set_flashdata('flash', '<div class="alert alert-success" role="alert">
        <h2>Selamat Datang Admin!</h2> </div>');

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/v_admin', $data);
        $this->load->view('templates/admin/footer');
    }

    public function inputdata()                               //----------------------------------------------------------------------///
    {
        $data['title'] = 'Tambah Data';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['tbl_paket'] = $this->M_crud->tampil_data()->row_array();

        $this->form_validation->set_rules('paket_tour', 'Paket_tour', 'required|trim');
        $this->form_validation->set_rules('tujuan', 'Tujuan', 'required|trim');
        $this->form_validation->set_rules('one_day', 'One_day', 'required|trim');
        $this->form_validation->set_rules('n2D1N', 'n2D1N', 'required|trim');
        $this->form_validation->set_rules('n3D2N', 'n3D2N', 'required|trim');
        $this->form_validation->set_rules('n4D3N', 'n4D3N', 'required|trim');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('admin/view/inputdata', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $config['upload_path']          = './upload/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2048;
            $config['max_width']            = 5000;
            $config['max_height']           = 5000;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image')) {

                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
            $this->M_crud->tambahDataPaket();

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                      <h2>Sukses!</h2> 
                    <p>Data berhasil dimasukkan</p>
                    <hr></div>');
            redirect('admin');
        }
    }
    public function hapus($id)
    {
        $this->M_crud->hapusPaket($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Data Terhapus!</h2></div>');
        redirect('admin');
    }
    public function edit($id)                               //-----------------------------------------------------------------//
    {
        $this->load->library('form_validation');

        $data['title'] = 'Ubah Data';
        $data['tbl_paket'] = $this->M_crud->tampilById($id);
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('paket_tour', 'required|trim');
        $this->form_validation->set_rules('tujuan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('admin/view/edit', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $paket = $this->input->post('paket_tour');
            $tujuan = $this->input->post('tujuan');
            $harga = $this->input->post('harga');
            $keterangan = $this->input->post('keterangan');

            $this->db->set('paket_tour', $paket);
            $this->db->set('tujuan', $tujuan);
            $this->db->set('harga', $harga);
            $this->db->set('keterangan', $keterangan);
            $this->db->where('id', $id);
            $this->db->update('tbl_paket');
            redirect('admin');
        }
    }
    public function simpanEdit()
    {                       //---------------------------------------------------------------------------------//
        $this->M_crud->editDataPaket();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h1>Data Berhasil Diubah!!!<h1></div>');
        redirect('admin');
    }
    public function role()
    {
        $data['title'] = 'Role';
        $data['role'] = $this->db->get('user_role')->result_array();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $this->session->set_flashdata('flash', '<div class="alert alert-success" role="alert">
        <h2>Selamat Datang Admin!</h2> </div>');

        $this->form_validation->set_rules('role', 'Role', 'Required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('admin/view/role', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $this->db->insert('user_role', ['role' => $this->input->post('role')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Role Baru Berhasil Ditambahkan! </div>');
            redirect('admin/role');
        }
    }
    public function deleteRole($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_role');
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Role Terhapus!</h2></div>');
        redirect('admin/role');
    }


    public function roleakses($role_id)
    {
        $data['title'] = 'Role Akses';
        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('menu_user')->result_array();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $this->session->set_flashdata('flash', '<div class="alert alert-success" role="alert">
        <h2>Selamat Datang Admin!</h2> </div>');

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/roleakses', $data);
        $this->load->view('templates/admin/footer', $data);
    }
    public function ubahAkses()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->nuw_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h1>Akses Diubah<h1></div>');
    }
    public function daftarPemesananPromo()
    {
        $data['title'] = 'Pemesanan Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['pemesananPromo'] = $this->db->get('checkout_promo')->result_array();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/pemesananPromo', $data);
        $this->load->view('templates/admin/footer');
    }
    public function hapusCheckoutPromo($id)
    {
        $this->M_crud->hapusCheckoutPromo($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Pesanan Terhapus!</h2></div>');
        redirect('admin/daftarPemesananPromo');
    }
}
