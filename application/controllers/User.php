<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_user');
        $this->load->library('form_validation');
        $this->load->model('Model_about', 'about');
        cek_login();
    }
    public function index()
    {
        $data['title'] = 'Ardian Tour';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['about'] = $this->about->tampilAbout()->result();

        $jumlah_data = $this->M_user->getAll()->num_rows();
        $config['base_url'] = 'http://localhost/ardian/auth#pakettour/';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 4;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);


        $data['tbl_paket'] = $this->M_user->getAllPaket($config['per_page'], $from)->result(); //Mengambil data perpage
        $this->load->view('templates/user/user_header', $data);
        $this->load->view('v_user', $data);
        $this->load->view('templates/user/user_footer', $data);
    }
    public function ubahPassword()
    {
        $data['title'] = 'Ubah Password';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[4]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', ' Confirm New Password', 'required|trim|min_length[4]|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('admin/view/ubahpass', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Lama Salah</div>');
                redirect('user/ubahPassword');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password tidak boleh sama dengan password sebelumnya</div>');
                    redirect('user/ubahPassword');
                } else {
                    $hash = password_hash($new_password, PASSWORD_DEFAULT);

                    $this->db->set('password', $hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user');
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password Berhasil Diubah</div>');
                    redirect('user/ubahPassword');
                }
            }
        }
    }
}
