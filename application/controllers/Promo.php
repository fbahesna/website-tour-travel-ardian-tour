<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promo extends CI_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Model_promo');
        $this->load->Model('M_Transaksi');
        $this->load->Model('M_crud');
        $this->db->get_where('user', ['email' => $this->input->post('email')])->row_array();
    }
    public function index()
    {
        $data['title'] = 'Halaman Promo';
        $data['promo'] = $this->Model_promo->getAllPromo()->result();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/user/user_header', $data);
        $this->load->view('promo/v_promo', $data);
        $this->load->view('templates/user/user_footer');
    }
    public function settingsPromo()
    {
        $data['title'] = 'Settings Promo';
        $data['promo'] = $this->Model_promo->getAllPromo()->result();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('promo/v_promo_admin', $data);
        $this->load->view('templates/admin/footer');
    }

    public function tambahPromo()
    {
        $data['title'] = 'Tambah Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('nama_promo', 'NamaPromo', 'required|trim');
        $this->form_validation->set_rules('bulan_promo', 'BulanPromo', 'required|trim');
        $this->form_validation->set_rules('harga_promo', 'HargaPromo', 'required|trim');
        $this->form_validation->set_rules('potongan_promo', 'PotonganPromo', 'required|trim');
        $this->form_validation->set_rules('durasi_promo', 'DurasiPromo', 'required|trim');
        $this->form_validation->set_rules('pesan_promo', 'PesanPromo', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('promo/tambah_promo');
            $this->load->view('templates/admin/footer');
        } else {
            $config['upload_path']          = './UploadPromo/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2048;
            $config['max_width']            = 5000;
            $config['max_height']           = 5000;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image')) {

                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            } else {
                echo $this->upload->display_errors();
            }

            $this->Model_promo->tambahPromo();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h2>Berhasil Ditambahkan</h2></div>');
            redirect('Promo/settingsPromo');
        }
    }
    public function hapusPromo($id)
    {
        $this->Model_promo->hapusPromo($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Data Terhapus!</h2></div>');
        redirect('Promo/settingsPromo');
    }
    public function editPromo($id)
    {
        $data['title'] = 'Edit Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['promo'] = $this->Model_promo->getById($id);

        $this->form_validation->set_rules('nama_promo', 'NamaPromo', 'required|trim');
        $this->form_validation->set_rules('bulan_promo', 'BulanPromo', 'required|trim');
        $this->form_validation->set_rules('harga_promo', 'HargaPromo', 'required|trim');
        $this->form_validation->set_rules('potongan_promo', 'PotonganPromo', 'required|trim');
        $this->form_validation->set_rules('pesan_promo', 'PesanPromo', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('promo/edit_promo', $data);
            $this->load->view('templates/admin/footer');
        }
    }
    public function simpanEditPromo()
    {
        $this->Model_promo->editPromo();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h2>Berhasil Di Edit </h2></div>');
        redirect('Promo/settingsPromo');
    }

    public function ambilPromo($id)
    {
        $data['title'] = 'Konfirmasi Pengambilan Promo';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['promo'] = $this->Model_promo->getById($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('notelepon', 'Notelepon', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('qty', 'Qty', 'required|trim');

        if (!$this->form_validation->run()) {
            $this->load->view('templates/user/user_header', $data);
            $this->load->view('promo/ambilPromo', $data);
            $this->load->view('templates/user/user_footer');
        } else {
            $this->Model_promo->ambilPromo();
        }
    }
    public function showPromo()
    {
        $data['title2'] = 'Rincian Pesanan Promo';
        $data['title'] = '';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['sesiPromo'] = $this->db->get_where('checkout_promo', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('promo/pemesananUserPromo', $data);
        $this->load->view('templates/admin/footer');
    }
    public function cancelPromo()
    {
        $this->Model_promo->cancelPromo();
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Pemesanan promo anda berhasil dibatalkan!</h2></div>');
        redirect('promo/showPromo');
    }
}
