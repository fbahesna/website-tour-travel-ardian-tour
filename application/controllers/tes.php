<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tes extends CI_Controller

{
    function index()
    {
        $this->load->model('M_tes');
        $b['data'] = $this->M_tes->tampil();
        $this->load->view('V_tes', $b);
    }
}
