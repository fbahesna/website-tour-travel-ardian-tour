<?php
defined('BASEPATH') or exit('No direct script access allowed');

class paketAll extends CI_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Model_paketAll');
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $jumlah_data = $this->Model_paketAll->tampildata()->num_rows();
        $config['base_url'] = 'http://localhost/ardianTour/paketAll/index';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 6;
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        $from = $this->uri->segment(3);

        $data['pagination'] = $this->pagination->create_links();

        $data['paket'] = $this->Model_paketAll->tampilDataPagination($config['per_page'], $from)->result();

        $this->load->view('templates/user/user_header', $data);
        $this->load->view('paketAll/view_paketAll', $data);
        $this->load->view('templates/user/user_footer');
    }
}
