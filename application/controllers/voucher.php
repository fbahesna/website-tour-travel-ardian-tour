<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Voucher extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_voucher', 'V');
    }

    public function index()
    {
        $data['title']  = 'VOUCHER';
        $data['getVoucher'] = $this->V->getVoucher();
        $data['voucher'] = $this->V->getNoVoucher();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['cekInputVoucher'] = $this->db->get_where('voucher', ['diskon' => null]);


        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/view_voucher', $data);
        $this->load->view('templates/admin/footer', $data);
    }
    public function getData()
    {
        $data = $this->V->getVoucher();
        echo json_encode($data);
    }
    public function saveVoucher()
    {
        $this->V->saveVoucher();
        $this->session->set_flashdata('flash2', '<h5 class="text-success" >Kode Kupon Berhasil Dibuat</h5>');
        redirect('voucher');
    }
    public function hapusVoucher($id)
    {
        $this->V->hapus($id);
        $this->session->set_flashdata('message', '<h4 class="text-danger" >Voucher Terhapus!</h4>');
        redirect('voucher');
    }
    public function editVoucher()
    {
        $data['title']  = 'VOUCHER';
        $data['getVoucher'] = $this->V->getVoucher();
        $data['voucher'] = $this->V->getNoVoucher();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['cekInputVoucher'] = $this->db->get_where('voucher', ['diskon' => null]);

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/view_voucher', $data);
        $this->load->view('templates/admin/footer', $data);
    }
    public function ambilVoucher()
    {
        $data['title'] = 'Voucher Wisata';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['getVoucher'] = $this->V->getVoucher();
        // $data['expiredDate'] = $this->V->expired();

        $this->load->view('templates/user/user_header', $data);
        $this->load->view('admin/view/view_voucher_user', $data);
        $this->load->view('templates/user/user_footer', $data);
    }
}
