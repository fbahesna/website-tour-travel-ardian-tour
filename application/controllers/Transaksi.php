<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf_report');
        $this->load->model('M_Transaksi');
        if (!$this->session->userdata('email')) {
            redirect('auth/login');
        }
    }
    public function index($id)
    {
        $data['title'] = ' Preview Transaksi';
        // $url_cetak = 'transaksi/cetak';
        $data['transaksi'] = $this->M_Transaksi->tampilByid($id);

        // $data['url_cetak'] = base_url('index.php/' . $url_cetak);
        $this->load->view('print', $data);
    }
    public function cetakPromo($id)
    {
        $data['title'] = "Preview Transaksi";
        $data['transaksiPromo'] = $this->M_Transaksi->tampilPromoById($id);
        $this->load->view('printPromo', $data);
    }
}
