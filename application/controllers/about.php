<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_about', 'model');
    }
    public function index()
    {
        $data['title'] = 'halaman about';
        $data['about'] = $this->model->tampilAbout()->row_array();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/user/user_header', $data);
        $this->load->view('about/view_about', $data);
        $this->load->view('templates/user/user_footer');
    }
    public function editAbout()
    {
        $data['about'] = $this->model->tampilAbout()->row_array();

        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');
        $this->form_validation->set_rules('about', 'About', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/user/user_header', $data);
            $this->load->view('about/view_about', $data);
            $this->load->view('templates/user/user_footer');
        } else {
            $this->model->editAbout();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> 
            <p>Edit Berhasil</p>
            <hr></div');
            redirect('about');
        }
    }

    public function clientSendEmail()
    {
        $data['title'] = 'Kirim Pesan';
        // $data['about'] = $this->model->tampilAbout()->result_array();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('email', 'mail', 'required|trim');
        $this->form_validation->set_rules('subject', 'subjek', 'required|trim');
        $this->form_validation->set_rules('pesan', 'Pesan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/user/user_header', $data);
            $this->load->view('user/userSendEmail');
            $this->load->view('templates/user/user_footer', $data);
        } else {
            $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_user' => 'ardiantour46@gmail.com',
                'smtp_pass' => 'Ardiantour123',
                'smtp_port' => 465,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n"
            ];
            $this->load->library('email', $config);
            $this->email->initialize($config);
            $this->email->from($this->session->userdata('email'));
            $this->email->to('ardiantour46@gmail.com', 'Ardian Tour');
            $this->email->subject($this->input->post('subject'));
            $this->email->message($this->input->post('pesan'));

            if ($this->email->send()) {
                return true;
            } else {
                echo $this->email->print_debugger();
                die;
            }
        }
    }
    public function cekEmail()
    {
        $data['title'] = 'Pesan Masuk';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['pesan'] = $this->model->cekEmail();
        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/cekEmail', $data);
        $this->load->view('templates/admin/footer');
    }
    public function hapusPesanMasuk($id)
    {
        $this->model->hapusPesan($id);
        redirect('about/cekEmail');
    }
    public function sendEmail()
    {
        $this->model->sendEmail();
        $this->session->set_flashdata('pesan', '<h3 class="text-success">Pesan Berhasil Terkirim</h3>');
        redirect('about');
    }
}
