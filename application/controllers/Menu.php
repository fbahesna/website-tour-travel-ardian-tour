<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_menu');
        if (!$this->session->userdata('email')) {
            redirect('auth/blocked');
        }
        if ($this->session->userdata('role_id') == 2) {
            redirect('auth/blocked');
        }
    }
    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('menu_user')->result_array();

        $this->form_validation->set_rules('menu', 'Menu', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $this->db->insert('menu_user', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Menu Baru Berhasil Ditambahkan! </div>');
            redirect('menu');
        }
    }
    public function delete($id)
    {
        $this->m_menu->hapusMenu($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><h2>Menu Terhapus!</h2></div>');
        redirect('menu');
    }




    public function submenu()
    {

        $data['title'] = 'SubMenu Management';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->model('m_menu', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('menu_user')->result_array();


        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');


        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><h2>Berhasil Menambahkan Submenu Baru!</h2></div>');
            redirect('menu/submenu');
        }
    }
    public function hapusSubMenu($id)
    {
        $this->m_menu->hapusSubMenu($id);
        $this->session->set_flashdata('flash', '<div class="alert alert-danger" role="alert"><h2>Sub Menu Terhapus!!!</h2></div>');
        redirect('menu/submenu');
    }


    public function editsubmenu()
    {
        $data['title'] = 'SubMenu Management';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->model('m_menu', 'menu');

        $data['menu'] = $this->db->get('menu_user')->result_array();


        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');


        if ($this->form_validation->run() == false) {
            $this->load->view('templates/admin/header', $data);
            $this->load->view('templates/admin/sidebar', $data);
            $this->load->view('templates/admin/topbar', $data);
            $this->load->view('menu/editsubmenu', $data);
            $this->load->view('templates/admin/footer');
        } else {
            $this->menu->editSubMenu();
            $this->session->set_flashdata('flash', '<div class="alert alert-danger" role="alert"><h2>Berhasil Diubah</h2></div>');
            redirect('menu/submenu');
        }
    }
}
