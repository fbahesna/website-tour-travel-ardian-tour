<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_user');
        // $this->defaultPage();
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }
        $jumlah_data = $this->M_user->getAll()->num_rows(); //Mengambil data sebaris
        $config['base_url'] = 'http://localhost/ardian/auth#pakettour/';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 4;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);
        $data['judul'] = 'Beranda';
        $data['tbl_paket'] = $this->M_user->getAllPaket($config['per_page'], $from)->result(); //Mengambil data perpage
        $this->load->view('templates/user/user_header', $data);
        $this->load->view('index', $data);
    }

    public function login()
    {
        $this->defaultPage();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Halaman Login';
            $this->load->view('templates/auth/auth_header', $data);
            $this->load->view('auth/view/login');
            $this->load->view('templates/auth/auth_footer');
        } else {
            //ketika validasinya success,agar tidak bisa diakses oleh url
            $this->login_();
        }
    }

    private function login_()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->db->get_where('user', ['email' => $email])->row_array(); //query ke db mencari kecocokan user dan email sesuai yang di tulis di form regist


        //jika usernya ada 
        if ($user) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    $this->session->set_userdata($data);
                    if ($user['role_id'] == 1) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        <h2>Selamat Datang Admin!</h2> </div>');
                        redirect('admin');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        <h2>Selamat Datang User  </h2> </div>');
                        redirect('user');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Password salah! </div>');
                    redirect('auth/login');
                }
            }
        } else {
            //jika email tidak ada
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email belum terdaftar! Silahkan daftar terlebih dahulu. </div>');
            redirect('auth/login');
        }
    }

    public function registration()
    {
        $this->defaultPage();

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This email has already registered!!!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|matches[password2]', [ //validasi ke 2 pass
            'matches' => 'password dont match!', 'min_length' => 'password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Halaman Registration';
            $this->load->view('templates/auth/auth_header', $data);
            $this->load->view('auth/view/registration');
            $this->load->view('templates/auth/auth_footer');
        } else {
            $data = [
                'nama ' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 1,
                'date_created' => time()
            ];
            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Selamat!</h4>
            <p>Akun anda telah berhasil dibuat.Silahkan login agar dapat melakukan pemesanan paket tour!</p> 
            <hr></div>'); //notif pesan ketika akun telah berhasil dibuat!!!
            redirect('auth');
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            
            <h2>Anda telah keluar!</h2> 
            <p>Silahkan Masuk kembali untuk pemesanan</p>
            <hr></div>');
        redirect('auth');
    }

    public function defaultPage()
    {
        if ($this->session->userdata('role_id') == 1) {
            redirect('admin');
        } else if ($this->session->userdata('role_id') == 2) {
            redirect('user');
        }
    }
    public function blocked()
    {
        $data['user'] = $this->M_user->getUser()->row_array();
        $this->load->view('templates/user/user_header', $data);
        $this->load->view('auth/view/blocked');
        $this->load->view('templates/user/user_footer', $data);
    }
}
