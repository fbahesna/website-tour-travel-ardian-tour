<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pesan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_crud');
        $this->load->model('M_Transaksi', 'transaksi');
        $this->load->model('Model_promo');
        $data['promo'] = $this->Model_promo->getAllPromo();
    }
    public function konfirmasiPesan($id)
    {
        $data['tbl_paket'] = $this->M_crud->tampilById($id);
        $data['title'] =  'Rincian Pesan';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();


        $this->load->view('templates/user/user_header', $data);
        $this->load->view('pesan/v_pesan', $data);
        $this->load->view('templates/user/user_footer');
    }
    public function CheckOut($id)
    {
        $data['tbl_paket'] = $this->M_crud->tampilById($id);
        $data['title'] = 'Check Out Pemesanan';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['durasi'] = $this->db->get_where('tbl_paket', ['id' => $id])->row_array();


        $this->form_validation->set_rules('namalengkap', 'Namalengkap', 'Required|trim');
        $this->form_validation->set_rules('email', 'Email', 'Required|trim|valid_email');
        $this->form_validation->set_rules('notelepon', 'Notelepon', 'Required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'Required|trim');
        $this->form_validation->set_rules('durasi', 'Durasi', 'Required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/user/user_header', $data);
            $this->load->view('pesan/check_out', $data);
            $this->load->view('templates/user/user_footer');
        } else {
            //validasi voucher
            if ($this->input->post('voucher') != "") {
                $this->db->select('*');
                $this->db->where('id', $this->input->post('voucher'));
                $this->db->limit(1); // only apply if you have more than same id in your table othre wise comment this line
                $query = $this->db->get('voucher');
                $potongan = $query->row();
            } else {
                $diskon = 0;
                $potongan = 0;
            }
            if (empty($potongan)) {
                $diskon = 0;
            } else {
                $diskon = $potongan->diskon;
            }
            $totalpembayaran = (int) $this->input->post('durasi') * (int) $this->input->post('qty') - $diskon;
            $data = [
                "tujuan_wisata" => $this->input->post('tujuan_wisata', true),
                "namalengkap" => $this->input->post('namalengkap', true),
                "email" => $this->input->post('email', true),
                "notelepon" => $this->input->post('notelepon', true),
                "alamat" => $this->input->post('alamat', true),
                "date_created" => date("d-m-Y"),
                "durasi" => $this->input->post('durasi', true),
                "qty" => $this->input->post('qty', true),
                "date" => $this->input->post('start', true),
                "voucher" => $this->input->post('voucher', true),
                "total_bayar" =>  $totalpembayaran
            ];
            $cek_email = $this->transaksi->cekEmail();
            if ($cek_email == false) {
                $this->db->insert('checkout', $data);
                $insert_id = $this->db->insert_id();
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <h2 class="alert-heading">Selamat!</h2>
                <p>Pemesanan anda telah kami terima,periksa email anda untuk mencetak barang bukti pemesanan anda</p> 
                <hr></div>');
                $this->showConfirm($insert_id);
                $this->_kirimEmail();
            } else {
                $this->load->model('M_user');
                $data['user'] = $this->M_user->getUser()->row_array();
                $this->load->view('templates/user/user_header', $data);
                $this->load->view('pesan/blockedOrder');
            }
        }
    }
    // kirim email
    private function _kirimEmail()
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'ardiantour46@gmail.com',
            'smtp_pass' => 'Ardiantour123',
            'smtp_port' => 465,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->from('ardiantour46@gmail.com', 'Ardian Tour');
        $this->email->to($this->input->post('email'));
        $this->email->subject('Pemesanan Paket Wisata');
        $message = 'Pemesanan telah diproses,admin akan segera menghubungi dan mengkonfirmasi pesanan.<hr/>
        Data anda sudah masuk ke database kami.<hr/>Terima kasih telah mempercayai kami sebagai penyedia layanan jasa tour wisata.<hr/>';
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function showConfirm($id)
    {
        $this->db->select('*');
        $this->db->where('id', $this->input->post('voucher'));
        $this->db->limit(1); // only apply if you have more than same id in your table othre wise comment this line
        $query = $this->db->get('voucher');
        $data['potongan'] = $query->row();

        $data['pemesanan'] = $this->db->get('checkout')->result_array();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Rincian Pesanan';
        $data['cek'] = $this->db->get_where('promo', ['durasi_promo' => $this->input->post('durasi_promo')])->row_array();

        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->limit(1); // only apply if you have more than same id in your table othre wise comment this line
        $query = $this->db->get('checkout');
        $checkout = $query->row();
        // print_r($checkout);
        // die();
        $data['checkout'] = $checkout;

        $this->load->view('templates/user/user_header', $data);
        $this->load->view('pesan/confirm', $data);
        $this->load->view('templates/admin/footer');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                       <h5 class="alert-heading">Selamat!</h5>
                       <p>Pemesanan anda telah kami terima,periksa email anda untuk mencetak barang bukti pemesanan anda</p> 
                       <hr></div>');
    }
    public function tampilBySesi()
    {
        $this->db->select('*');
        $this->db->where('id', $this->input->post('voucher'));
        $this->db->limit(1); // only apply if you have more than same id in your table othre wise comment this line
        $query = $this->db->get('voucher');
        $data['potongan'] = $query->row();

        $data['proses'] = base_url('pesan/prosesPesanan');
        $data['title'] = 'Pesanan Anda';
        $data['title2'] = 'Pesanan Promo';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['durasi'] = $this->db->get('tbl_paket')->row_array();
        $data['belumPesan'] = $this->db->get('checkout', ['email' => $this->session->userdata('email')])->result_array();
        $data['sesi'] = $this->transaksi->cekEmail();
        $data['cek'] = $this->db->get_where('promo', ['durasi_promo' => $this->input->post('durasi_promo')])->row_array();
        $data['sesiPromo'] = $this->db->get_where('checkout_promo', ['email' => $this->session->userdata('email')])->row_array();
        // $dataPesan = $this->db->get_where('checkout', ['email' => $this->session->userdata('email')])->row_array();
        // $dataPromo = $this->db->get_where('checkout_promo', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('pesan/pemesananUser', $data);
        $this->load->view('promo/pemesananUserPromo', $data);
        $this->load->view('templates/admin/footer');
    }
    public function deleteConfirm($id)
    {
        $this->Model_promo->getById($id);

        $this->db->delete('checkout', ['email' => $this->session->userdata('email')]);
        $this->session->set_flashdata('message', 'Pesanan Dibatalkan!');
        redirect('pesan/tampilBySesi');
    }
    public function tampilCheckout()
    {
        $this->db->select('*');
        $this->db->where('id', $this->input->post('voucher'));
        $this->db->limit(1); // only apply if you have more than same id in your table othre wise comment this line
        $query = $this->db->get('voucher');
        $data['potongan'] = $query->row();

        $data['title'] = 'Pemesanan Paket';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['durasi'] = $this->db->get('tbl_paket')->row_array();
        $data['pemesanan'] = $this->db->get('checkout')->result_array(); //Langsung query untuk mengambil semua data pada tabel checkout
        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/pemesanan', $data);
        $this->load->view('templates/admin/footer');
    }
    public function hapusCheckout($id)
    {
        $this->trqansaksi->hapusCheckout($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            
            <h4>Pemesanan Berhasil Dibatalkan!</h4> 
            <hr></div>');
        redirect('pesan/tampilCheckout');
    }
    public function editCheckout($id)
    {
        $this->load->model('M_Transaksi');

        $data['title'] = 'Ubah Pesanan';
        $data['checkout'] = $this->M_Transaksi->tampilById($id);
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/editPesanan', $data);
    }
    public function simpanEditCheckout($id)
    {
        $this->load->model('M_Transaksi');
        $this->M_Transaksi->editCheckout($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    
                    <h2>Pemesanan Berhasil Diubah</h2> 
                    <hr></div>');
        redirect('pesan/tampilCheckout');
    }
    public function prosesPesanan($id)
    {
        $data['title'] = 'Proses Pesanan Paket';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['laporan'] = $this->transaksi->prosesPesanan($id);

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/cekProses', $data);
    }
    public function proses()
    {
        $d = $this->transaksi->proses();
        $this->session->set_flashdata('flash', '<h4>Pesanan Diproses</h4>');
        redirect('pesan/laporanPesanan');
    }
    public function laporanPesanan()
    {
        $data['voucher'] = $this->db->get_where('checkout', ['voucher' => NULL]);
        $data['title'] = 'Laporan Pesanan Paket';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['laporanPesanan'] = $this->transaksi->laporanPesanan();

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar', $data);
        $this->load->view('templates/admin/topbar', $data);
        $this->load->view('admin/view/dataLaporan', $data);
        $this->load->view('templates/admin/footer', $data);
    }
}
